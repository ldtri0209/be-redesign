 <?php

/**

 * The front page template file

 *

 * If the user has selected a static page for their homepage, this is what will

 * appear.

 * Learn more: https://codex.wordpress.org/Template_Hierarchy

 *

 * @package WordPress

 * @subpackage Twenty_Seventeen

 * @since 1.0

 * @version 1.0

 */



get_header(); ?>



 <div id="dctrans-banner" class="carousel slide" data-ride="carousel"><!--Dctrans Banner-->

    <!-- Indicators -->

    <ol class="carousel-indicators">

      <li data-target="#dctrans-banner" data-slide-to="0" class="active"></li>

      <li data-target="#dctrans-banner" data-slide-to="1"></li>

      <li data-target="#dctrans-banner" data-slide-to="2"></li>

    </ol>

    <!-- Wrapper for slides -->

    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="<?php echo  get_template_directory_uri() ?>/images/banner1.jpg" alt="...">
        <div class="carousel-caption dctrans-carousel-title">
          <?php _e('Được thành lập từ năm 2003 Dịch vụ vận chuyển hàng Trung Quốc của Dctrans chuyên đang dần khẳng định được vị thế trong ngành vận chuyển hàng hóa và là lựa chọn top đầu của các doanh nghiệp.', 'dctrans_translate') ?>
        </div>
      </div>
      <div class="item">
        <img src="<?php echo  get_template_directory_uri() ?>/images/banner2.jpg" alt="...">
        <div class="carousel-caption dctrans-carousel-title">
          <div class="strong-title"><?php _e('DCTRANS Đồng hành cùng quý khách - Vươn tới mọi thành công', 'dctrans_translate') ?></div>
          <?php _e('DCtrans tự hào là một trong những nhân tố góp phần tạo nên thành công của khách hàng. DCtrans được đông đảo khách hàng tín nhiệm trong việc làm cầu nối vận chuyển hàng cho khách hàng và người tiêu dùng...', 'dctrans_translate') ?>
        </div>
      </div>
      <div class="item">
        <img src="<?php echo  get_template_directory_uri() ?>/images/banner3.jpg" alt="...">
        <div class="carousel-caption dctrans-carousel-title">
          <?php _e('Thế kỷ 21, bạn chỉ cần chiếc laptop, PC hay thậm chí là một chiếc smartphone cũng có thể mua hàng trên bất cứ đâu. Và việc mua hàng trung quốc, order taobao đặt hàng Trung Quốc không còn là xa lạ, không còn là khó khăn khi đến với chúng tôi DCtrans. 
          <br/>Bạn sẽ được hướng dẫn từ A-Z cho quá trình mua và đặt hàng của mình hoàn toàn miễn phí. ', 'dctrans_translate') ?>
        </div>
      </div>
    </div>

    <!-- Controls -->

    <a class="left carousel-control" href="#dctrans-banner" role="button" data-slide="prev">

      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>

      <span class="sr-only"><?php _e('Previous', 'dctrans_translate') ?></span>

    </a>

    <a class="right carousel-control" href="#dctrans-banner" role="button" data-slide="next">

      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>

      <span class="sr-only"><?php _e('Next', 'dctrans_translate') ?></span>

    </a>

  </div><!--End DCtrans Banner-->

  <div class="clear-fix"></div>



  <div class="shipping-calculator container"><!--////// Shipping Calculator : Writen by : Huy Truong August 12, 2017 -->

    <div class="col-md-6 shipping-left">

      <ul>

        <li class="steps"><img src="<?php echo  get_template_directory_uri() ?>/images/steps.png" /></li>

        <li class="title"><?php _e('Tạo đơn hàng nhanh chóng chỉ với hai bước', 'dctrans_translate') ?></li>

        <li class="step-des"><span class="bold"><?php _e('Bước 1', 'dctrans_translate') ?>:</span> <?php _e('Lựa chọn dịch vụ vận chuyển đường biển hoặc đường bộ, điền các thông số bắt buộc', 'dctrans_translate') ?></li>

        <li class="step-des"><span class="bold"><?php _e('Bước 2', 'dctrans_translate') ?>:</span> <?php _e('Kiểm tra kết quả tính toán từ hệ thống và đặt đơn hàng', 'dctrans_translate') ?></li>

      </ul>

    </div>

    <div class="col-md-6 shipping-right">

      <div class="calculate-table">

      

        <!-- Nav tabs -->

        <ul class="nav nav-tabs" role="tablist">

          <li role="presentation" class="active">

            <a href="#home" aria-controls="home" role="tab" data-toggle="tab" class="tab-title">

              <i class="fa fa-truck" aria-hidden="true"></i> <?php _e('Đường bộ', 'dctrans_translate') ?></a>

          </li>

          <li role="presentation">

            <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" class="tab-title">

              <i class="fa fa-archive" aria-hidden="true"></i> <?php _e('Đường biển', 'dctrans_translate') ?></a>

          </li>

        </ul>



        <!-- Tab panes -->

        <div class="tab-content">

          <div role="tabpanel" class="tab-pane active cal-panel" id="home">

              <form action="" method="" id="frm-track-by-land">

            <ul class="tab-cal-ul">

              <li class="cal-txt">

                <label><?php _e('Lựa chọn địa điểm', 'dctrans_translate') ?></label>

                <select class="form-control location-select"  name="location">

                  <option value="hcm"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hồ Chí Minh', 'dctrans_translate') ?></option>

                  <option value="hn"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hà Nội', 'dctrans_translate') ?></option>

                </select>

              </li>

              <li class="cal-txt">

                <label><?php _e('Trọng lượng hàng (kg)', 'dctrans_translate') ?></label>

                <input type="text" class="form-control" name="p-weight" placeholder="<?php _e('nhập trọng lượng', 'dctrans_translate') ?>">

              </li>

              <li class="cal-txt">

                <label><?php _e('Dịch vụ thêm', 'dctrans_translate') ?></label>

                <select class="form-control location-select" name="dong_goi"  >

                  <option value=""><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Lựa chọn dịch vụ thêm', 'dctrans_translate') ?></option>

                 

                  <option value="khong_dong"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Không đóng gói', 'dctrans_translate') ?></option>

                  <option value="dong_bao"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Đóng bao', 'dctrans_translate') ?></option>

                  <option value="thung_go"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Đóng thùng gỗ', 'dctrans_translate') ?></option>

                </select>

              </li>

              <li class="cal-btn">

              <input type="hidden" id ="action" name="action" value="by_land" />

                <button class="btn btn-default dctrans-btn"><?php _e('Ước tính ngay', 'dctrans_translate') ?></button>

              </li>

            </ul>

            </form>

            <div id="div_detail-by-land"></div>

          </div>

          <div role="tabpanel" class="tab-pane cal-panel" id="profile">

          <form action="" method="" id="frm-track-by-sea">

            <ul class="tab-cal-ul">

              <li class="cal-txt">

                <label><?php _e('Lựa chọn địa điểm', 'dctrans_translate') ?></label>

                <select class="form-control location-select" name="location">

                  <option value="hcm"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hồ Chí Minh', 'dctrans_translate') ?></option>

                  <option value="hn"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hà Nội', 'dctrans_translate') ?></option>

                </select>

              </li>

              <li class="cal-txt">

                <div class="col-md-6 segment-left">

                  <label><?php _e('Trọng lượng hàng (kg)', 'dctrans_translate') ?></label>

                  <input type="text" class="form-control"  name="p-weight" placeholder="nhập trọng lượng hàng">

                </div>

                <div class="col-md-6 segment-right">

                  <label><?php _e('Thể tích hàng hoá (m3)', 'dctrans_translate') ?></label>

                  <input type="text" class="form-control"    name="category_thetich" placeholder="nhập thể tích hàng">

                </div>

                <div class="clear-fix"></divL>

              </li>

              <li class="cal-txt">

                <label><?php _e('Loại hàng hoá', 'dctrans_translate') ?></label>

                <select class="form-control location-select" name="category_hang" id="mathang">

                  <option value="thongthuong"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hàng thông thường', 'dctrans_translate') ?></option>

                  <option value="vai"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Các loại vải', 'dctrans_translate') ?></option>

                  <option value="dientu"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hàng điện tử', 'dctrans_translate') ?></option>

                </select>

              </li>



              <li class="cal-btn">

                <input type="hidden" id ="action" name="action" value="by_sea" />

                <button class="btn btn-default dctrans-btn"><?php _e('Ước tính ngay', 'dctrans_translate') ?></button>

              </li>

            </ul>



          </form>

          <div id="div_detail-by-sea"></div>



          </div>

        </div>

      </div>



    </div>

  </div>

  <!--///////// 

  //////////// End : Shipping Calculator ///////////////

  ////////////-->



  <div class="parallax">

    <div class="container-fluid home-services-container"><!--////// Our Strong Points : Writen by : Huy Truong August 12, 2017 -->

      <div class="container sub-home">

        <div class="clear-fix"></div>

        <div class="home-services-title">

          <?php _e('Dịch vụ vận chuyển đáng tin cậy ', 'dctrans_translate') ?>

        </div>

        <div class="home-services-des">

          <?php _e('Chúng tôi gồm bốn điểm mạnh ', 'dctrans_translate') ?>

        </div>

        <div class="home-services-listicons">



        <!-- Huy Truong: This section is displaying how does strong points look

        /////////////////// -->

          <div class="col-md-3 listicons-cell">

            <ul class="listicons-ul">

              <li><img class="rep-icon" src="<?php echo  get_template_directory_uri() ?>/images/option1.png" /></li>

              <li class="title"><?php _e('HỖ TRỢ NHANH CHÓNG', 'dctrans_translate') ?></li>

              <li class="content"><?php _e('Chúng tôi tự hào có đội ngũ nhân viên tư vấn, CSKH chuyên nghiệp, hỗ trợ dịch vụ tốt nhất. Tư vấn hoàn toàn miễn phí.', 'dctrans_translate') ?></li>

            <ul>

          </div>



          <div class="col-md-3 listicons-cell">

            <ul class="listicons-ul">

              <li><img class="rep-icon" src="<?php echo  get_template_directory_uri() ?>/images/option2.png" /></li>

              <li class="title"><?php _e('PHẠM VI PHỦ SÓNG', 'dctrans_translate') ?></li>

              <li class="content"><?php _e('Dịch vụ vận chuyển hàng Trung Quốc về Việt Nam của Dctrans hiện đang có mặt tại hầu hết các tỉnh thành của Trung Quốc', 'dctrans_translate') ?><span class="read_more">...<?php _e('Xem thêm', 'dctrans_translate') ?></span> <span class="readmore" style="display: none;"> <?php _e('Khách hàng đặt hàng ở bất kỳ tỉnh thành nào chúng tôi vận nhận được hàng và chuyển giao đến tận tay khách hàng tại Việt Nam', 'dctrans_translate') ?></span></li>

            <ul>

          </div>

          <div class="col-md-3 listicons-cell">

            <ul class="listicons-ul">

              <li><img class="rep-icon" src="<?php echo  get_template_directory_uri() ?>/images/option3.png" /></li>

             <li class="title"><?php _e('GIAO HÀNG TẬN TAY', 'dctrans_translate') ?></li>

            <li class="content"><?php _e('Dù là vận chuyển đường bộ hay đường biển.Dù khách hàng ở HN, HCM hay các tỉnh thành khác. Dctrans sẽ giao hàng đến tận', 'dctrans_translate') ?> <span class="read_more2">...<?php _e('Xem thêm', 'dctrans_translate') ?></span>  <span class="readmore2" style="display: none;"><?php _e('nhà của khách hàng. Để đảm bảo hàng hóa đến tay khách hàng an toàn, nguyên vẹn', 'dctrans_translate') ?>.</span> </li>

            <ul>

          </div>

          <div class="col-md-3 listicons-cell">

            <ul class="listicons-ul">

              <li><img class="rep-icon" src="<?php echo  get_template_directory_uri() ?>/images/option4.png" /></li>

              <li class="title"><?php _e('GIÁ CẢ PHẢI CHẲNG', 'dctrans_translate') ?></li>

              <li class="content"><?php _e('Đến với vận chuyển hàng DCtrans, chúng tôi đảm bảo mức giá thấp nhất, có lợi nhất cho khách hàng. Phí dịch vụ = 0%', 'dctrans_translate') ?></li>

            <ul>

          </div>

        </div>



        <div class="col-md-8 col-md-offset-2 col-xs-12 home-tracking-code"><!--////// Tracking Tool section : Writen by : Huy Truong August 12, 2017 -->

            <div class="col-md-6 col-xs-12 home-tracking-left">

              <ul>

                <li class="title"><?php _e('KIỂM TRA HÀNG?', 'dctrans_translate') ?></li>

                <li class="content" style="min-height: 36px;"><?php _e('Nhập mã theo dõi của bạn vào đây để kiểm tra tình trạng hàng hóa của bạn', 'dctrans_translate') ?></li>

              </ul>

            </div>

            <div class="col-md-6 col-xs-12 home-tracking-right">

                  <form action='/tracking-page' method='post' target='_blank'  class="form-search">

                    <div class="input-group ht-search-group">

                      <input type="hidden" name="ewd-otp-action" value="track">

                      <input type="text" id="ewd-otp-tracking-number" name="Tracking_Number" placeholder="<?php _e('Nhập mã theo dõi', 'dctrans_translate') ?>" style="padding: 6px">

                      <span class="input-group-btn">

                          <button type="submit" class="btn"><?php _e('Tìm ngay', 'dctrans_translate') ?></button>

                      </span>

                    </div>

                  </form>

            </div>

        </div><!-- End tracking tool-->

      </div>

    </div><!--END: Our Strong Points-->





    <div class="container-fluid main-services-container"> <!-- Main services container -->

      <div class="container-fluid title-container">

        <div class="title"><?php _e('Dịch vụ của chúng tôi', 'dctrans_translate') ?></div>

        <div class="description"><?php _e('Chúng tôi cung cấp ba dịch vụ chính sau', 'dctrans_translate') ?> </div>

      </div>

      <div class="container sub-main">

        <div class="col-md-4 mainservices-col">

              <ul class="mainservices-ul">

                  <li class="image"><img src="<?php echo  get_template_directory_uri() ?>/images/service1.png" /></li>

                  <li class="title"><?php _e('Vận chuyển Taobao', 'dctrans_translate') ?></li>

                  <li class="content">

                      <?php _e('DCtrans hỗ trợ tư vấn miễn phí, hướng dẫn tạo tài khoản mua hàng, đặt hàng không qua trung gian - KHÔNG MẤT PHÍ DỊCH VỤ. Tối ưu lợi nhuận cho quý khách hàng.', 'dctrans_translate') ?>

                  </li>

                  <li class="action">

                      <a class="btn btn-default mainservices-btn" href="http://dctrans.asia/taobao" ><?php _e('XEM THÊM', 'dctrans_translate') ?></a>

                  </li>

              </ul>

        </div>

        <div class="col-md-4 mainservices-col">

              <ul class="mainservices-ul">

                  <li class="image"><img src="<?php echo  get_template_directory_uri() ?>/images/service2.png" /></li>

                  <li class="title"><?php _e('Vận chuyển bằng đường biển', 'dctrans_translate') ?></li>

                  <li class="content">

                      <?php _e('Với những mặt hàng có kích thước lớn, cồng kềnh và dễ vỡ như đồ nội thất, xây dựng, hàng điện tử… Vận chuyển hàng Trung Quốc về Việt Nam là lựa chọn tối ưu cho quý khách.', 'dctrans_translate') ?>

                  </li>

                  <li class="action">

                      <a class="btn btn-default mainservices-btn" href="http://dctrans.asia/dich-vu-van-chuyen-duong-bien/" ><?php _e('XEM THÊM', 'dctrans_translate') ?></a>

                  </li>

              </ul>

        </div>

        <div class="col-md-4 mainservices-col">

            <ul class="mainservices-ul">

                <li class="image"><img src="<?php echo  get_template_directory_uri() ?>/images/service3.png" /></li>

                <li class="title"><?php _e('Vận chuyển bằng đường bộ', 'dctrans_translate') ?></li>

                <li class="content">

                    <?php _e('DCtrans vận chuyển hàng từ Trung Quốc về Việt Nam bằng đường bộ với thời gian vận chuyển nhanh, giá thành thấp. DCtrans giúp khách hàng kinh doanh, buôn bán dễ dàng hơn.', 'dctrans_translate') ?>

                </li>

                <li class="action">

                      <a class="btn btn-default mainservices-btn" href="http://dctrans.asia/dich-vu-van-chuyen-duong-bo/" ><?php _e('XEM THÊM', 'dctrans_translate') ?></a>

                </li>

            </ul>

        </div>

        <div class="clear-fix"></div>



      </div>



      </div>

    </div><!-- End main service container -->

  </div>

  <!--///////// 

  //////////// End : Parallax Section ///////////////

  ////////////-->



   <div class="clear-fix"></div>





  <div class="container-fluid ourpartner-container"><!--Our partners Section-->

    <div class="container">

      <div class="ourpartner-title"><?php _e('Đối tác của chúng tôi', 'dctrans_translate') ?></div>

      <div class="partner-logos-container">

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/alipay.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/alibaba.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/tencent.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/dchotel.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/zara.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/kohler.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/visa.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/union.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/maer.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/kangaroo.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/vingroup.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/hoabinh.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/cneastern.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/huawei.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/cntelecom.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/tma.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/gongcha.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/lazada.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/vivo.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/taobao.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/sacombank.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/vietcombank.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/verizon.png" /></div>

        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/cisco.png" /></div>

      </div>

    </div>

  </div><!--/////// End our partner section-->

<?php get_footer();?>



