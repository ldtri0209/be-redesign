<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

	<section class="panelscrool"  id="home-top" data-section-name="whychooseus" data-section-id="Why Choose Us">
		
		<div class="container-fluid home-services-container">
    <div class="container sub-home">
      <div class="col-md-8 col-md-offset-2 col-xs-12 home-tracking-code"><!-- Tracking Tool-->
          <div class="col-md-6 col-xs-12 home-tracking-left">
            <ul>
            	<?php if(ICL_LANGUAGE_CODE=='vi'): ?>
					<li class="title">KIỂM TRA HÀNG?</li>
              		<li class="content">Nhập mã theo dõi của bạn vào đây để kiểm tra tình trạng hàng hóa của bạn</li>
				<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
				<li class="title">Where my cargo?</li>
				<li class="title">Enter your tracking code here to check your cargo status</li>
				<?php endif;?>

             
            </ul>
          </div>
          <div class="col-md-6 col-xs-12 home-tracking-right">
            
            	<?php if(ICL_LANGUAGE_CODE=='vi'): ?>
					<form action='/tracking-page' method='post' target='_blank'  class="form-search input-group ht-search-group">
		              <input type="text" class="form-control" placeholder="Nhập mã theo dõi...">
		              <span class="input-group-btn">
                <button class="btn btn-secondary search-btn" type="button">Tìm ngay</button>
              </span>
              </form>
				<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
					<form action='/zh-hant/tracking-page' method='post' target='_blank'  class="form-search input-group ht-search-group">
		              <input type="text" class="form-control" placeholder="Nhập mã theo dõi...">
		              <span class="input-group-btn">
                <button class="btn btn-secondary search-btn" type="button">Search</button>
              </span>
              </form>
				<?php endif;?>
            	
           
          </div>
      </div><!-- End tracking tool-->
      <div class="clear-fix"></div>
      <?php if(ICL_LANGUAGE_CODE=='vi'): ?>
				<div class="home-services-title">
   					Dịch vụ vận chuyển đáng tin cậy 
			      </div>
			      <div class="home-services-des">
			        Chúng tôi gồm bốn điểm mạnh 
			      </div>
			     
			<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
				<div class="home-services-title">
   					Why choose us
			      </div>
			      <div class="home-services-des">
			       We strike with 4 strong points
			      </div>
			
		<?php endif;?>
							
      

      <div class="home-services-listicons row"><!--* * * Listicon Instance -->
        <div class="col-md-3 col-xs-6 listicons-cell">
          <ul class="listicons-ul">
            <li><img class="rep-icon" src="<?php echo  get_template_directory_uri() ?>/assets2/images/option1.png" /></li>
            <?php if(ICL_LANGUAGE_CODE=='vi'): ?>
				 <li class="title">HỖ TRỢ NHANH CHÓNG</li>
            	 <li class="content">Chúng tôi tự hào có đội ngũ nhân viên tư vấn, CSKH chuyên nghiệp, hỗ trợ dịch vụ tốt nhất. Tư vấn hoàn toàn miễn phí.</li>
				<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
				<li class="content">On time support</li>
				<div class="info">
					Strategy is a high level plan to achieve one or more goals under conditions of uncertainty. In the sense of the 
				</li>
			<?php endif;?>
           
          </ul>
        </div>

        <div class="col-md-3 col-xs-6 listicons-cell">
          <ul class="listicons-ul">
            <li><img class="rep-icon" src="<?php echo  get_template_directory_uri() ?>/assets2/images/option2.png" /></li>
            <?php if(ICL_LANGUAGE_CODE=='vi'): ?>
				<li class="title">PHẠM VI PHỦ SÓNG</li>
           		 <li class="content">Dịch vụ vận chuyển hàng Trung Quốc về Việt Nam của Dctrans hiện đang có mặt tại hầu hết các tỉnh thành của Trung Quốc<span class="read_more">...Xem thêm</span> <span class="readmore" style="display: none;"> Khách hàng đặt hàng ở bất kỳ tỉnh thành nào chúng tôi vận nhận được hàng và chuyển giao đến tận tay khách hàng tại Việt Nam</span></li>
				<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
				<li class="title">Tracking & Reports</li>
				<li class="content">
					Strategy is a high level plan to achieve one or more goals under conditions of uncertainty.
				</li>
			<?php endif;?>
            
          </ul>
        </div>
        <div class="col-md-3 col-xs-6 listicons-cell">
          <ul class="listicons-ul">
            <li><img class="rep-icon" src="<?php echo  get_template_directory_uri() ?>/assets2/images/option3.png" /></li>
            <?php if(ICL_LANGUAGE_CODE=='vi'): ?>
            	 <li class="title">GIAO HÀNG TẬN TAY</li>
            <li class="content">Dù là vận chuyển đường bộ hay đường biển.Dù khách hàng ở HN, HCM hay các tỉnh thành khác. Dctrans sẽ giao hàng đến tận <span class="read_more2">...Xem thêm</span>  <span class="readmore2" style="display: none;">nhà của khách hàng. Để đảm bảo hàng hóa đến tay khách hàng an toàn, nguyên vẹn.</span> </li>
				
				<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
				 <li class="title">Packge Projected</li>
				 <li class="content">
					Management in businesses and organizations is the function that coordinates the efforts of people to
				</li>
			<?php endif;?>
           
          <ul>
        </div>
        <div class="col-md-3 col-xs-6 listicons-cell">
          <ul class="listicons-ul">
            <li><img class="rep-icon" src="<?php echo  get_template_directory_uri() ?>/assets2/images/option4.png" /></li>
            <?php if(ICL_LANGUAGE_CODE=='vi'): ?>
				 <li class="title">GIÁ CẢ PHẢI CHẲNG</li>
           		 <li class="content">Đến với vận chuyển hàng DCtrans, chúng tôi đảm bảo mức giá thấp nhất, có lợi nhất cho khách hàng. Phí dịch vụ = 0%</li>
				<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
				 <li class="title">Lowest price</li>
				 <li class="content">
					Management in businesses and organizations is the function that coordinates the efforts of people to accomplish 
				</li>
				<?php endif;?>
           
          </ul>
        </div>

      </div>

    </div>
	</div>





<!--	
		<div class="inner">
			<div class="vertical-center">
				<div class="container">
					<div class="second-head">
						<div class="col-md-7 col-sm-7 col-xs-12">
							<div class="img"><img src="<?php echo  get_template_directory_uri() ?>/assets2/imgs/2.png" align=""></div>
							<div class="info">
							<?php if(ICL_LANGUAGE_CODE=='vi'): ?>
								<h2>Kiểm tra hàng?</h2>
								<h3>Nhập mã theo dõi của bạn vào đây để kiểm tra tình trạng hàng hóa của bạn</h3>
							<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
							<h2>Where my cargo?</h2>
							<h3>Enter your tracking code here to check your cargo status</h3>
							<?php endif;?>
								
							</div>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
						<?php if(ICL_LANGUAGE_CODE=='vi'): ?>
								<form action='/vi/tracking-page' method='post' target='_blank'  class="form-search">
								<input type="hidden" name="ewd-otp-action" value="track">
								<div class="input-control">
								<input type="text" id="ewd-otp-tracking-number" name="Tracking_Number" placeholder="Nhập mã theo dõi">
								<button type="submit" class="btn">Tìm</button>
								</div>
							</form>
							<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
							<form action='/zh-hant/tracking-page' method='post' target='_blank'  class="form-search">
								<input type="hidden" name="ewd-otp-action" value="track">
								<div class="input-control">
								<input type="text" id="ewd-otp-tracking-number" name="Tracking_Number" placeholder="Tracking Code">
								<button type="submit" class="btn">Search</button>
								</div>
							</form>
							<?php endif;?>
							
						</div>
						<div class="clear"></div>
					</div>

					<?php //echo do_shortcode( '[tracking-form]' ); ?>
					<div class="clear"></div>
					<div class="option">
						<div class="title">
						<?php if(ICL_LANGUAGE_CODE=='vi'): ?>
								<h2>DỊCH VỤ VẬN CHUYỂN ĐÁNG TIN CẬY CỦA BẠN</h2>
							<h3>CHÚNG TÔI GỒM 4 ĐIỂM MẠNH</h3>
							<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
							<h2>Why choose us</h2>
							<h3>We strike with 4 strong points</h3>
							<?php endif;?>
							
						</div>
						<div class="content">
							<div class="row">
								<div class="item col-md-3 col-sm-6 col-xs-6">
									<div class="img">
										<img src="<?php echo  get_template_directory_uri() ?>/assets2/imgs/option1.png" class="img-responsive" alt="">
									</div>
									<?php if(ICL_LANGUAGE_CODE=='vi'): ?>
									<h3>Hỗ trợ nhanh chóng </h3>
									<div class="info">
										Chúng tôi tự hào có đội ngũ nhân viên tư vấn, CSKH chuyên nghiệp, hỗ trợ dịch vụ tốt nhất. Tư vấn hoàn toàn miễn phí. 
									</div>
									<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
									<h3>On time support</h3>
									<div class="info">
										Strategy is a high level plan to achieve one or more goals under conditions of uncertainty. In the sense of the 
									</div>
									<?php endif;?>
									
								</div>
								<div class="item col-md-3 col-sm-6 col-xs-6">
									<div class="img">
										<img src="<?php echo  get_template_directory_uri() ?>/assets2/imgs/icon-ps.png" class="img-responsive" alt="">
									</div>
									<?php if(ICL_LANGUAGE_CODE=='vi'): ?>
									<h3>Phạm vi phủ sóng</h3>
									<div class="info">
										 <span class="more">Dịch vụ vận chuyển hàng Trung Quốc về Việt Nam của Dctrans hiện đang có mặt tại hầu hết các tỉnh thành của Trung Quốc. Khách hàng đặt hàng ở bất kỳ tỉnh thành nào chúng tôi vận nhận được hàng và chuyển giao đến tận tay khách hàng tại Việt Nam. </span>
									</div>
									<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
									<h3>Tracking & Reports</h3>
									<div class="info">
										Strategy is a high level plan to achieve one or more goals under conditions of uncertainty.
									</div>
									<?php endif;?>
									
								</div>
								<div class="item col-md-3 col-sm-6 col-xs-6">
									<div class="img">
										<img src="<?php echo  get_template_directory_uri() ?>/assets2/imgs/option3.png" class="img-responsive" alt="">
									</div>
									<?php if(ICL_LANGUAGE_CODE=='vi'): ?>
									<h3>Giao hàng tận tay</h3>
									<div class="info">
										 <span class="more">Dù là vận chuyển đường bộ hay đường biển.Dù khách hàng ở HN, HCM hay các tỉnh thành khác. Dctrans sẽ giao hàng đến tận tay quý khách, gõ cửa tận nhà của khách hàng. Để đảm bảo hàng hóa đến tay khách hàng an toàn, nguyên vẹn.</span>

									</div>
									<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
									<h3>Packge Projected</h3>
									<div class="info">
										Management in businesses and organizations is the function that coordinates the efforts of people to
									</div>
									<?php endif;?>
									
									
								</div>
								<div class="item col-md-3 col-sm-6 col-xs-6">
									<div class="img">
										<img src="<?php echo  get_template_directory_uri() ?>/assets2/imgs/option4.png" class="img-responsive" alt="">
									</div>
									<?php if(ICL_LANGUAGE_CODE=='vi'): ?>
									<h3>Lowest price</h3>
									<div class="info">
										Đến với vận chuyển hàng DCtrans, chúng tôi đảm bảo mức giá thấp nhất, có lợi nhất cho khách hàng. Phí dịch vụ  = 0% 

									</div>
									<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
									<h3>Lowest price</h3>
									<div class="info">
										Management in businesses and organizations is the function that coordinates the efforts of people to accomplish 
									</div>
									<?php endif;?>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
-->
	</section>
	<section class="panelscrool"  id="dich-vu" data-section-name="whatwedo" data-section-id="What We Do">
	
		<div class="inner">
			<div class="vertical-center">
				<div class="container">
					<div class="option option-1">
						<div class="title">
							<?php if(ICL_LANGUAGE_CODE=='vi'): ?>
							<h2>Dịch vụ của chúng tôi</h2>
							<h3>Chúng tôi cung cấp 3 dịch vụ chính</h3>
							<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
							<h2>What we do</h2>
							<h3>We provide 3 server</h3>
							<?php endif;?>

						</div>
						<div class="content">
							<div class="row">

							<div class="pic3">
								<div class="item col-md-4 col-sm-4 col-xs-12">
									
									<div class="img-wwd">
										<img src="<?php echo  get_template_directory_uri() ?>/assets2/imgs/tabao.png" class="img-responsive" alt="">																	
										
										<?php if(ICL_LANGUAGE_CODE=='vi'): ?>
										<div class="h3-wwd"><h3 class="h3-1 h3-vi">Vận chuyển Taobao</h3></div>

											<div class="input-control-wwd">									
												<a class="wwd wwd-taobao" href="http://dctrans.asia/vi/taobao/">XEM THÊM</a>
											</div>	
										<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
										<div class="h3-wwd"><h3 class="h3-1">TAOBAO DELIVERY</h3></div>

											<div class="input-control-wwd">									
												<a class="wwd wwd-taobao" href="http://dctrans.asia/taobao/">FIND OUT MORE</a>
											</div>		
										<?php endif;?>

																																									
										
									</div>
																	
								</div>									


								<div class="item col-md-4 col-sm-4 col-xs-12">
									
									<div class="img-wwd">
										<img src="<?php echo  get_template_directory_uri() ?>/assets2/imgs/sea.png" class="img-responsive" alt="">																	
										
											<?php if(ICL_LANGUAGE_CODE=='vi'): ?>
										<div class="h3-wwd"><h3 class="h3-1 h3-vi ">Vận chuyển bằng đưởng biển</h3></div>

											<div class="input-control-wwd">									
												<a class="wwd wwd-taobao" href="http://dctrans.asia/vi/our-services/#sea_box">XEM THÊM</a>
											</div>	
										<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
									<div class="h3-wwd">	<h3 class="h3-2">SEA TRANSPORTATION</h3></div>								
											<div class="input-control-wwd">									
												<a class="wwd wwd-taobao" href="http://dctrans.asia/our-services/#sea_box">FIND OUT MORE</a>
											</div>		
										<?php endif;?>

																				
									
									</div>
									
								</div>

								<div class="item col-md-4 col-sm-4 col-xs-12">
									
									<div class="img-wwd">
										<img src="<?php echo  get_template_directory_uri() ?>/assets2/imgs/ground.png" class="img-responsive" alt="">																	
										
										<?php if(ICL_LANGUAGE_CODE=='vi'): ?>
											<div class="h3-wwd">
											<h3 class="h3-1 h3-vi">Vận chuyển bằng đường bộ</h3>
											</div>
											<div class="input-control-wwd">									
												<a class="wwd wwd-taobao" href="http://dctrans.asia/vi/our-services/#ground_box">XEM THÊM</a>
											</div>	
										<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
										<div class="h3-wwd"><h3 class="h3-3">GROUND TRANSPORTATION</h3>	</div>							
											<div class="input-control-wwd">									
												<a class="wwd wwd-taobao" href="http://dctrans.asia/our-services/#ground_box">FIND OUT MORE</a>
											</div>		
										<?php endif;?>
																					
										
									</div>
								
								</div>
							</div>



							<!--	<div class="item col-md-4 col-sm-4 col-xs-12">
									<div class="img">
										<img src="<?php //echo  get_template_directory_uri() ?>/assets2/imgs/5.png" class="img-responsive" alt="">
									</div>
									<h3>24/7 Transportation</h3>
									<div class="info">
										We have a highly trained and diverse team of CAD architects with experience delivering UK…
									</div>
								</div>
								<div class="item col-md-4 col-sm-4 col-xs-12">
									<div class="img">
										<img src="<?php //echo  get_template_directory_uri() ?>/assets2/imgs/6.png" class="img-responsive" alt="">
									</div>
									<h3>Big Warehouse</h3>
									<div class="info">
										Our team of BIM experts are experienced in delivering a broad range of size of projects…
									</div>
								</div>
								<div class="item col-md-4 col-sm-4 col-xs-12">
									<div class="img">
										<img src="<?php //echo  get_template_directory_uri() ?>/assets2/imgs/7.png" class="img-responsive" alt="">
									</div>
									<h3>Worldwide Delivery</h3>
									<div class="info">
										Sed ut perspiciatis unde omnis iste natus error sit voluptatem accu Sed ut perspicia …
									</div>
								</div>-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="panelscrool" id="doi-tac" data-section-name="whatpeoplesay" data-section-id="What People Say">
		<div class="inner">
			<div class="vertical-center container-fluid ourpartner-container">
				<div class="container">
					<div class="people_say">
						<div class="title">
						<?php if(ICL_LANGUAGE_CODE=='vi'): ?>
							<h2 style="color: #FFF">ĐỐI TÁC CỦA CHÚNG TÔI</h2>
							
							<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
							<h2>OUR PARTNERSHIP</h2>
							
							<?php endif;?>
							
						</div>
						<div class="">
							<div class="owl-people-dt partner-logos-container">

							<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/alipay.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/alibaba.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/tencent.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/dchotel.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/zara.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/kohler.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/visa.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/union.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/maer.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/kangaroo.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/vingroup.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/hoabinh.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/cneastern.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/huawei.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/cntelecom.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/tma.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/gongcha.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/lazada.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/vivo.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/taobao.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/sacombank.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/vietcombank.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/verizon.png" /></div>
				<div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/assets2/images/logos/cisco.png" /></div>
               		<!-- <div class="item">

							<?php  /*
		
	                $args  =  array(
	                        'post_type'      => 'partners',
	                        'orderby'    => 'ASC',
	                        'posts_per_page' => -1,
	                      );
                 $loop = new WP_Query($args); ?>
                 <?php while ( $loop->have_posts() ) : $loop->the_post(); 
                 	 $link = get_field('link',$post->ID);
                  ?>
                  

						
							<div class="col-md-2 col-xs-4 logo"><div class="img-dt">
								<a href="#"><?php echo (has_post_thumbnail()) ? get_the_post_thumbnail($post->ID, 'full',array( 'class' => 'img-responsive' )) : ''; ?></a>
							</div></div>
							
						
				<?php endwhile; wp_reset_query(); */ ?>
							</div>	 !-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php get_footer();
