<?php

/**

 * The template for displaying all pages

 *

 * This is the template that displays all pages by default.

 * Please note that this is the WordPress construct of pages

 * and that other 'pages' on your WordPress site may use a

 * different template.

 *

 * @link https://codex.wordpress.org/Template_Hierarchy

 *

 * @package WordPress

 * @subpackage Twenty_Seventeen

 * @since 1.0

 * @version 1.0

 */



get_header();



$admin_email = get_option('admin_email') ? get_option('admin_email') : 'contact@logistic.com';

$display = wp_get_current_user()->user_firstname . " " . wp_get_current_user()->user_lastname;

?>



	<div class="dctrans-newscenter-container">



		<div class="newsletter">

			<div class="container">

				<span class="caption"> <?php _e('Hãy đăng ký thành viên để được cập nhật tin tức mỗi ngày', 'dctrans_translate')?></span>

				<button class="btn btn-default dctrans-btn"><?php _e('Đăng ký ngay', 'dctrans_translate')?>!</button>

			</div>

		</div>



		<div class="container newscenter-head">

			<div class="title"><?php _e('Trang tin tức', 'dctrans_translate')?></div>

			<div class="description"><?php _e('Cập nhật những tin tức mới nhất từ DCtrans', 'dctrans_translate')?></div>

		</div>

		<div class="container news-body">



			<?php



			$args  =  array(

				'post_type'      => 'post',

				'orderby'    => 'DESC',

				'paged' => get_query_var('page'),

				'posts_per_page' => 6,

			);



			$loop = new WP_Query($args); ?>

			<?php while ( $loop->have_posts() ) : $loop->the_post();



				?>



				<div class="news-cell col-md-4">

					<ul class="news-ul">

						<li class="image">

							<a class="outbound-img" href="<?php the_permalink(); ?>">

								<?php echo (has_post_thumbnail()) ? get_the_post_thumbnail($post->ID, 'recent-size-2',array( 'class' => 'img-responsive-2' )) : ''; ?>

							</a>

						</li>

						<li class="date">

							<?php echo get_the_date(); ?>

						</li>

						<li class="title">

							<a href="<?php the_permalink(); ?>"><?php _e(wp_trim_words( get_the_title(), 12), 'dctrans_translate')?></a>

						</li>

					</ul>

				</div>



			<?php endwhile; wp_reset_query(); ?>



		</div>

		<div class="dctrans-pagination">

			<div class="pagenavi">

				<?php wp_pagenavi(array( 'query' => $loop ));?>

			</div>

		</div>

	</div>









	<div class="shipping-calculator container"><!--////// Shipping Calculator : Writen by : Huy Truong August 12, 2017 -->

		<div class="col-md-6 shipping-left">

			<ul>

				<li class="steps"><img src="<?php echo  get_template_directory_uri() ?>/images/steps.png" /></li>

				<li class="title"><?php _e('Tạo đơn hàng nhanh chóng chỉ với hai bước', 'dctrans_translate')?></li>

				<li class="step-des"><span class="bold"><?php _e('Bước 1', 'dctrans_translate')?>:</span> <?php _e('Lựa chọn dịch vụ vận chuyển đường biển hoặc đường bộ, điền các thông số bắt buộc', 'dctrans_translate')?></li>

				<li class="step-des"><span class="bold"><?php _e('Bước 2', 'dctrans_translate')?>:</span> <?php _e('Kiểm tra kết quả tính toán từ hệ thống và đặt đơn hàng', 'dctrans_translate')?></li>

			</ul>

		</div>

		<div class="col-md-6 shipping-right">

			<div class="calculate-table">



				<!-- Nav tabs -->

				<ul class="nav nav-tabs" role="tablist">

					<li role="presentation" class="active">

						<a href="#home" aria-controls="home" role="tab" data-toggle="tab" class="tab-title">

							<i class="fa fa-truck" aria-hidden="true"></i> <?php _e('Đường bộ', 'dctrans_translate')?></a>

					</li>

					<li role="presentation">

						<a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" class="tab-title">

							<i class="fa fa-archive" aria-hidden="true"></i> <?php _e('Đường biển', 'dctrans_translate')?></a>

					</li>

				</ul>



				<!-- Tab panes -->

				<div class="tab-content">

					<div role="tabpanel" class="tab-pane active cal-panel" id="home">

						<form action="" method="" id="frm-track-by-land">

							<ul class="tab-cal-ul">

								<li class="cal-txt">

									<label><?php _e('Lựa chọn địa điểm', 'dctrans_translate')?></label>

									<select class="form-control location-select"  name="location">

										<option value="hcm"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hồ Chí Minh', 'dctrans_translate')?></option>

										<option value="hn"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hà Nội', 'dctrans_translate')?></option>

									</select>

								</li>

								<li class="cal-txt">

									<label><?php _e('Trọng lượng hàng (kg)', 'dctrans_translate')?></label>

									<input type="text" class="form-control" name="p-weight" placeholder="nhập trọng lượng">

								</li>

								<li class="cal-txt">

									<label><?php _e('Dịch vụ thêm', 'dctrans_translate')?></label>

									<select class="form-control location-select" name="dong_goi"  >

										<option value=""><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Lựa chọn dịch vụ thêm', 'dctrans_translate')?></option>



										<option value="khong_dong"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Không đóng gói', 'dctrans_translate')?></option>

										<option value="dong_bao"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Đóng bao', 'dctrans_translate')?></option>

										<option value="thung_go"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Đóng thùng gỗ', 'dctrans_translate')?></option>

									</select>

								</li>

								<li class="cal-btn">

									<input type="hidden" id ="action" name="action" value="by_land" />

									<button class="btn btn-default dctrans-btn"><?php _e('Ước tính ngay', 'dctrans_translate')?></button>

								</li>

							</ul>

						</form>

						<div id="div_detail-by-land"></div>

					</div>

					<div role="tabpanel" class="tab-pane cal-panel" id="profile">

						<form action="" method="" id="frm-track-by-sea">

							<ul class="tab-cal-ul">

								<li class="cal-txt">

									<label><?php _e('Lựa chọn địa điểm', 'dctrans_translate')?></label>

									<select class="form-control location-select" name="location">

										<option value="hcm"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hồ Chí Minh', 'dctrans_translate')?></option>

										<option value="hn"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hà Nội', 'dctrans_translate')?></option>

									</select>

								</li>

								<li class="cal-txt">

									<div class="col-md-6 segment-left">

										<label><?php _e('Trọng lượng hàng (kg)', 'dctrans_translate')?></label>

										<input type="text" class="form-control"  name="p-weight" placeholder="<?php _e('nhập trọng lượng hàng', 'dctrans_translate')?>">

									</div>

									<div class="col-md-6 segment-right">

										<label><?php _e('Thể tích hàng hoá (m3)', 'dctrans_translate')?></label>

										<input type="text" class="form-control"    name="category_thetich" placeholder="<?php _e('nhập thể tích hàng', 'dctrans_translate')?>">

									</div>

									<div class="clear-fix"></div>

								</li>

								<li class="cal-txt">

									<label><?php _e('Loại hàng hoá', 'dctrans_translate')?></label>

									<select class="form-control location-select" name="category_hang" id="mathang">

										<option value="thongthuong"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hàng thông thường', 'dctrans_translate')?></option>

										<option value="vai"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Các loại vải', 'dctrans_translate')?></option>

										<option value="dientu"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hàng điện tử', 'dctrans_translate')?></option>

									</select>

								</li>



								<li class="cal-btn">

									<input type="hidden" id ="action" name="action" value="by_sea" />

									<button class="btn btn-default dctrans-btn"><?php _e('Ước tính ngay', 'dctrans_translate')?></button>

								</li>

							</ul>



						</form>

						<div id="div_detail-by-sea"></div>



					</div>

				</div>

			</div>



		</div>

	</div>

	<!--/////////

    //////////// End : Shipping Calculator ///////////////

    ////////////-->





<?php get_footer();