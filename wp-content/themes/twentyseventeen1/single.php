<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<section class=" home single " data-section-name="Sevices" data-section-id="" >
		<div class="header-second">
			<div class="container-fluid">
				<div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="">Intlogistic</a></div>
				<a  class="phone-top" href="tel:03723399874" title=""><i class="zmdi zmdi-hc-fw"></i> <span>037-2339-9874</span></a>
				<div class="search">
					<form action="/tracking-page" method="post" target="_blank" class="pure-form pure-form-aligned">
						<input type="hidden" name="ewd-otp-action" value="track">
						<div class="input-control">
							<input type="text" name="Tracking_Number"  placeholder="YOUR TRACKING YOUR HERE">
							<button type="submit" class="btn"><i class="zmdi zmdi-hc-fw"></i></button>
						</div>
					</form>
				</div>
				<div class="menu-top">
				<?php wp_nav_menu( array( 'theme_location' => 'Top Menu', 'menu_class' => 'nav navbar-nav', 'container'=> null ,'menu' => 'menutop') ); ?>
					
				</div>
				<div class="clear"></div>
			</div>
		</div>
		
</section> 
<section class=" home ">
<div class="inner">
	<div class="vertical-center">
		<div class="container">
	  <div class="option">
		<div class="title">
			
			<a class ="link_page" href="<?php echo esc_url( get_page_link( 96 ) ); ?>"><h3><i class="zmdi zmdi-long-arrow-left"></i>NEWS CENTER</h3></a>
		</div>
		<div class="content">
			<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<?php while(have_posts()):the_post();?>
				<div class="img-news-sg">
						<?php echo (has_post_thumbnail()) ? the_post_thumbnail($post->ID, 'full') : ''; ?>	
				</div>
				<div class="info-news-sg">
						<?php the_title();?>
				</div>
				 <p class="news-date-sg"><?php echo get_the_date(); ?></p>
				 <p class="line-more-sg"></p>
			 	<div class="sg-content"><?php the_content()?></div>
			<?php endwhile;wp_reset_query();?>
			</div>
			</div>
		</div>
		</div>
		</div>
	</div>
</div>
</section>
<section class=" home ">
<div class="inner">
	<div class="vertical-center">
		<div class="container">
	  <div class="option">
		<?php  $id = get_the_ID();
		get_samepost_category($id ); 
		?>
		</div>
		</div>
	</div>
</div>
</section>
<?php get_footer();
