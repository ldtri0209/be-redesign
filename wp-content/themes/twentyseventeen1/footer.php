<?php

/**

 * The template for displaying the footer

 *

 * Contains the closing of the #content div and all content after.

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package WordPress

 * @subpackage Twenty_Seventeen

 * @since 1.0

 * @version 1.2

 */


$token_integrate = get_option('token_integrate');

?>

   <footer class="dctrans-footer">

    <div class="container">

      <div class="col-md-4 col-xs-12 foot-col">

        <ul class="footcol-ul">

          <li class="logo"><img src="<?php echo  get_template_directory_uri() ?>/images/dc-foot-logo.png" /></li>

          <li class="contact-txt"><span class="bold"><?php _e('Điện thoại', 'dctrans_translate') ?>:</span> 09 34 34 34 89</li>

          <li class="contact-txt"><span class="bold"><?php _e('Theo chúng tôi', 'dctrans_translate') ?>:</span> 

            <a href="#"><i class="fa fa-lg fa-envelope item" aria-hidden="true"></i></a>

            <a href="#"><i class="fa fa-lg fa-facebook-square item" aria-hidden="true"></i></a>

          <li>

          <li class="separator"></li>

          <li class="copyright"><span class="bold">© <?php _e('Bản quyền thuộc về DCtrans Ltd. 2017', 'dctrans_translate') ?></li>

        </ul>

      </div>



      <div class="col-md-4 col-xs-12 foot-col">

        <ul class="footcol-ul">

          <li class="contact-title"><?php _e('Trụ sở ở Việt Nam', 'dctrans_translate') ?></li>

          <li class="contact-txt"><?php _e('Lầu 5, tòa nhà Đại Thanh Bình, số 911 Nguyễn Trãi, phường 14, Quận 5, thành phố Hồ Chí Minh.', 'dctrans_translate') ?></li>

          <li class="contact-txt"><span class="bold"><?php _e('Điện thoại', 'dctrans_translate') ?>:</span> 13725471204</li>

        </ul>

      </div>

      <div class="col-md-4 col-xs-12 foot-col">

        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fdctrans.asia%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="240" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" target="_top"></iframe>

      </div>

    </div>

  </footer>

<!-- Modal: Contact Form -->
<div id="myModal" class="modal fade dccontact-form-con" role="dialog">
  <div class="modal-dialog dc-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="title"><?php _e('Gửi câu hỏi', 'dctrans_translate') ?></div>
      </div>
      <div class="modal-body">
        <input type="hidden" id="rating-email">
        <div class="col-md-6 contact-form">
          <div class="list-textfield">
          <?php echo do_shortcode('[contact-form-7 id="194" title="Form contact_poup"]'); ?>
          </div>
        </div>
        <div class="col-md-6 contact-info">
          <ul>
            <li><img src="<?php echo  get_template_directory_uri() ?>/images/contact-bg.jpg" /></li>
            <li>
              <span class="contact-label"><?php _e('Hotline', 'dctrans_translate') ?>:</span> (+84)9 34 34 34 89
            </li>
            <li>
              <span class="contact-label"><?php _e('Email', 'dctrans_translate') ?>:</span> support@dctrans.asia
            </li>
          </ul>
        </div>
      </div>     
    </div>
  </div>
</div>

</div>
<!-- Modal:contact success -->
<div id="contactSuccess" class="modal fade dccontact-form-con" role="dialog">
  <div class="modal-dialog dc-dialog modal-lg">
    <form action="get">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-6 contact-form">
            <div class="title result-title"><?php _e('Cảm ơn bạn đã gửi câu hỏi cho chúng tôi', 'dctrans_translate') ?></div>
            <div class="result-description">
              <?php _e('Chúng tôi sẽ liên hệ trả lời vấn đề của bạn trong thời gian sớm nhất.<br/>
              Xin hãy đánh giá cho dịch vụ chúng tôi!', 'dctrans_translate') ?>
            </div>
            <div class="contact-rating">
              <div class="starrr" id="star-rating"></div>
            </div>
          </div>
          <div class="col-md-6 contact-info">
            <ul>
              <li><img src="<?php echo  get_template_directory_uri() ?>/images/contact-success.jpg" /></li>
              <li></li>
            </ul>
          </div>
        </div>     
      </div>
    </form>
  </div>
</div>

<script src="<?php echo  get_template_directory_uri() ?>/assets2/bootstrap/js/jquery.min.js"></script>

<script src="<?php echo  get_template_directory_uri() ?>/assets/js/jquery-confirm.min.js"></script>

<script src="<?php echo  get_template_directory_uri() ?>/assets2/js/owlCarousel/owl.carousel.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo  get_template_directory_uri() ?>/assets/js/starrr.js"></script>

<script type="text/javascript" src="<?php echo  get_template_directory_uri() ?>/assets2/js/wow.min.js"></script>

<script type="text/javascript" src="<?php echo  get_template_directory_uri() ?>/assets2/js/jquery.scrollify.js"></script>

<script type="text/javascript" src="<?php echo  get_template_directory_uri() ?>/assets2/js/page.js?v=1.0.0"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<?php wp_footer(); ?>



<div class="overflow"></div>

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9180440;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-100455113-1', 'auto');
  ga('send', 'pageview');

</script>


<div id="fb-root"></div>

<script>(function(d, s, id) {

  var js, fjs = d.getElementsByTagName(s)[0];

  if (d.getElementById(id)) return;

  js = d.createElement(s); js.id = id;

  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10&appId=1120262924673379";

  fjs.parentNode.insertBefore(js, fjs);

}(document, 'script', 'facebook-jssdk'));</script>

<script type="text/javascript">

jQuery(document).ready(function(){ 

  var showChar = 110;  // How many characters are shown by default

  var ellipsestext = "...";

  var moretext = "Xem thêm>";

  var lesstext = "Thu gọn";

  jQuery('.more').each(function() {

      var content = jQuery(this).html();

      if(content.length > showChar) {

          var c = content.substr(0, showChar);

          var h = content.substr(showChar, content.length - showChar);

          var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

          jQuery(this).html(html);

      }

  });

  jQuery(".morelink").click(function(){

      if(jQuery(this).hasClass("less")) {

          jQuery(this).removeClass("less");

          jQuery(this).html(moretext);

      } else {

          jQuery(this).addClass("less");

          jQuery(this).html(lesstext);

      }

      jQuery(this).parent().prev().toggle();

      jQuery(this).prev().toggle();

      return false;

  });


  jQuery('#frm-track-by-land').submit(function(ev) {

      jQuery.ajax({

          url: "<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php ",

          data: jQuery("#frm-track-by-land").serialize(),

          type: "POST",

          beforeSend: function(){

              // jQuery("#loadingitme").show();

          },

          success: function(data) {

              jQuery("#loadingitme").hide();

              jQuery('#frm-track-by-land').css('display', 'none');

              jQuery("#div_detail-by-land").html(data);

          }

      });

      return false;

  });

  jQuery('#frm-track-by-sea').submit(function(ev) {

      jQuery.ajax({

          url: "<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php ",

          data: jQuery("#frm-track-by-sea").serialize(),

          type: "POST",

          beforeSend: function(){

              // jQuery("#loadingitme").show();

          },

          success: function(data) {

              jQuery("#loadingitme").hide();

              jQuery('#frm-track-by-sea').css('display', 'none');

              jQuery("#div_detail-by-sea").html(data);

          }

      });

      return false;

  });

  jQuery(".read_more").click(function(){

      jQuery(".readmore").show();

      jQuery(".read_more").css( "display", "none" );

  });

  jQuery(".read_more2").click(function(){

      jQuery(".readmore2").show();

      jQuery(".read_more2").css( "display", "none" );

  });

    jQuery(".btn_oder").click(function(event){

        event.preventDefault();

        var token_integrate = <?php echo $token_integrate?>;
        var href = $(this).attr('href');
        var valNew = href.split('/');
        var val = valNew[4].split('&');


        var postArray = [];
        for(var i=0;i<val.length;i++){
            var valN = val[i].split('=');

            postArray.push({
                "key": valN[0].replace('?',''),
                "value": valN[1]
            });
        }

        console.log({
            'token_intgrate': token_integrate,
            'data': postArray
        });

        $.post("http://api.dctrans.me/order/create", {
            'token_intgrate': token_integrate,
            'data': postArray
        });


    });

});

</script>


<script>

  window.fbAsyncInit = function() {

    FB.init({

      appId      : '120087665296254',

      cookie     : true,

      xfbml      : true,

      version    : 'v2.8'

    });

    FB.AppEvents.logPageView();   

  };



  (function(d, s, id){

     var js, fjs = d.getElementsByTagName(s)[0];

     if (d.getElementById(id)) {return;}

     js = d.createElement(s); js.id = id;

     js.src = "//connect.facebook.net/en_US/sdk.js";

     fjs.parentNode.insertBefore(js, fjs);

   }(document, 'script', 'facebook-jssdk'));

</script>

<script>
document.addEventListener( 'wpcf7mailsent', function( event ) {
    $('#myModal, #contactSuccess').modal('toggle');
}, false );

$('#star-rating').starrr({
  change: function(e, value){
    $('#contactSuccess').modal('toggle');
    value && $.ajax({
        url: ajax_object.ajax_url,
        type: 'post',
        data: {
            action: 'store_rating_value',
            star: value,
            email: $( "#rating-email" ).val()
        },
        success: function(res) {
          console.log(res)
        },
    })
  }
});
</script>

</body>

</html>

