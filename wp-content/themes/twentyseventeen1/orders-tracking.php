<?php

/*

Template Name: Orders Tracking

*/

get_header();

?>

<?php

$Statuses_Array = parseStatusLanguage();

if (!is_array($Statuses_Array)) {$Statuses_Array = array();}

$Locations_Array = get_option("EWD_OTP_Locations_Array");

if (!is_array($Locations_Array)) {$Locations_Array = array();}

$Allow_Order_Payments = get_option("EWD_OTP_Allow_Order_Payments");



$Order_Information_String = get_option("EWD_OTP_Order_Information");

$Order_Information = explode(",", $Order_Information_String);



?>



<?php

$Customers = $wpdb->get_results("SELECT * FROM $EWD_OTP_customers");

$Sales_Reps = $wpdb->get_results("SELECT * FROM $EWD_OTP_sales_reps");

if ($Sales_Rep_Only == "Yes") {

	$Current_User = wp_get_current_user();

	$Sql = "SELECT Sales_Rep_ID FROM $EWD_OTP_sales_reps WHERE Sales_Rep_WP_ID='" . $Current_User->ID . "'";

	$Sales_Rep_ID = $wpdb->get_var($Sql);

}



if (isset($_GET['Page'])) {$Page = $_GET['Page'];}

else {$Page = 1;}



$Sql = "SELECT * FROM $EWD_OTP_orders_table_name WHERE Order_ID!='0' ";


$myrows = $wpdb->get_results($Sql);

if ($Sales_Rep_Only == "Yes") {$TotalOrders = $wpdb->get_results("SELECT Order_ID FROM $EWD_OTP_orders_table_name WHERE Order_Display='Yes' AND Sales_Rep_ID='" . $Sales_Rep_ID . "'");}

else {$TotalOrders = $wpdb->get_results("SELECT Order_ID FROM $EWD_OTP_orders_table_name WHERE Order_Display='Yes'");}

$Number_of_Pages = ceil($wpdb->num_rows/20);

$Current_Page = "admin.php?page=EWD-OTP-options&DisplayPage=Orders";

if (isset($_REQUEST['OrderNumber'])) {$Current_Page .= "&OrderNumber=" . $_REQUEST['OrderNumber'];}

if (isset($_REQUEST['Show_Hidden_Orders'])) {$Current_Page .= "&Show_Hidden_Orders=" . $_REQUEST['OrderNumber'];}

if (isset($_GET['OrderBy'])) {$Current_Page_With_Order_By = $Current_Page . "&OrderBy=" .$_GET['OrderBy'] . "&Order=" . $_GET['Order'];}

else {$Current_Page_With_Order_By = $Current_Page;}

?>

<div class="header-second">
	<div class="container-fluid">
		<div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="">Intlogistic</a></div>
		<a  class="phone-top" href="tel:03723399874" title=""><i class="zmdi zmdi-hc-fw"></i> <span>037-2339-9874</span></a>
		<div class="search">
			<?php if(ICL_LANGUAGE_CODE=='vi'): ?>
				<form action="/vi/tracking-page" method="post" target="_blank" class="pure-form pure-form-aligned">
					<input type="hidden" name="ewd-otp-action" value="track">
					<div class="input-control">
						<input type="text" name="Tracking_Number"  placeholder="Nhập mã theo dõi...">
						<button type="submit" class="btn"><i class="zmdi zmdi-hc-fw"></i></button>
					</div>
				</form>
			<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
				<form action="/tracking-page" method="post" target="_blank" class="pure-form pure-form-aligned">
					<input type="hidden" name="ewd-otp-action" value="track">
					<div class="input-control">
						<input type="text" name="Tracking_Number"  placeholder="Input tracking code...">
						<button type="submit" class="btn"><i class="zmdi zmdi-hc-fw"></i></button>
					</div>
				</form>
			<?php endif;?>
		</div>
		<div class="menu-top">
		<?php wp_nav_menu( array( 'theme_location' => 'Top Menu', 'menu_class' => 'nav navbar-nav', 'container'=> null ,'menu' => 'menutop') ); ?>
			
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="manage-content">

	<div class="vertical-center">

		<div class="container">

			<div class="option">
				<div class="title">
					<?php if (in_array('editor', wp_get_current_user()->roles)) {?>
						<?php if(ICL_LANGUAGE_CODE=='vi'): ?>
							<h2 class="title-page"><?php _e('DANH SÁCH ĐƠN HÀNG', 'order-tracking'); ?></h2>
						<?php else: ?>
							<h2 class="title-page"><?php _e('RECENT SUMMARY ORDERS', 'order-tracking'); ?></h2>
						<?php endif;?>
					<?php } ?>
				</div>

				<div class="content">

					<div class="row">

						<div class="col-md-12 col-sm-12 col-xs-12">

						<?php if (in_array('editor', wp_get_current_user()->roles)) {?>

							<main id="main" class="site-main" role="main">

								<div class="ewd-dashboard-middle">

									<div id="col-full">

										<p class="um-notice" id="notifi" style="display: none"><i class="um-icon-ios-close-empty" onclick="jQuery(this).parent().fadeOut();"></i><span id="notification"></span></p>

										<br>

										<div class="col-xl-12 col-md-12 col-sm-12 col-xs-12 no-padding text-right float-right">

											<?php if(ICL_LANGUAGE_CODE=='vi'): ?>

												<button type="button" class="btn btn-primary default-btn" id="sync-data">ĐỒNG BỘ DỮ LIỆU GOOGLE SHEEET</button>

												<button type="button" class="btn btn-primary default-btn" data-toggle="modal" data-target="#createOrderModal">THÊM ORDER</button>

											<?php else: ?>

												<button type="button" class="btn btn-primary default-btn" id="sync-data">SYNC DATA</button>

												<button type="button" class="btn btn-primary default-btn" data-toggle="modal" data-target="#createOrderModal">ADD NEW ORDER</button>
											<?php endif;?>

										</div>

										<div class="col-xl-12 col-md-12 col-sm-12 col-xs-12 no-padding my-orders-table">

											<table class='table table-striped table-hover' id="order-management">

												<thead>

												<tr>

													<th><?php _e("Order Number", 'order-tracking'); ?></th>

													<th><?php _e("Name", 'order-tracking'); ?></th>

													<th><?php _e("Status", 'order-tracking'); ?></th>

													<th><?php _e("Latest Updated", 'order-tracking'); ?></th>

													<th><?php _e("Shipping Address", 'order-tracking'); ?></th>

													<th><?php _e("Action", 'order-tracking'); ?></th>

												</tr>

												</thead>

												<tbody>

												<?php

												if ($myrows) {

													foreach ($myrows as $Order) {

														$sts = ICL_LANGUAGE_CODE == 'vi' ? parseStatusLanguage($Order->Order_Status) : stripslashes($Order->Order_Status);

														echo "<tr id='Order" . $Order->Order_ID ."'>";

														echo "<td class='name column-name'>";

														echo "<strong>";

														echo "<a class='row-title' href='".$_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME']."/order-detail?Order_ID=" . $Order->Order_Number ."' title='Edit " . $Order->Order_Number . "'>" . $Order->Order_Number . "</a></strong>";

														echo "</td>";

														echo "<td class='name column-name'>" . stripslashes($Order->Order_Name) . "</td>";

														echo "<td class='status column-status'>" . $sts . "</td>";

														echo "<td class='updated column-updated'>" . stripslashes($Order->Order_Status_Updated) . "</td>";

														echo "<td class='updated column-shipping-info'>" . stripslashes($Order->Order_Shipping_Info) . "</td>";

														echo "<td class='updated column-action'>
															<i style='display: inline-block;
																	cursor: pointer;color: #8a0000;'
																	title='Order ".$Order->Order_Number."'
																	row-index='".$index."'
																class='um-icon-android-delete delete-order' order-id='".$Order->Order_ID."'></i>
														</td>";

														echo "</tr>";

													}

												} else {

													echo '<tr><td colspan="6" style="text-align: center">No have data</td></tr>';

												}

												?>

												</tbody>

											</table>

										</div>

										<br class="clear" />

									</div>

								</div>

							</main>
						<?php } else {?>
							<h2 class="text-center">Do not have permission access to this page!</h2>
						<?php } ?>

							<!-- #main -->

							<div id="createOrderModal" class="modal fade">

								<div class="modal-dialog">

									<div class="modal-content">

										<div class="modal-header">

											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>

											<h4 class="modal-title" id="myModalLabel">Thêm Đơn Hàng</h4>

										</div>

										<div class="modal-body">

											<input type="hidden" name="action" value="Add_Order" />

											<input type="hidden" name="Order_Display" value="Yes">

											<?php wp_nonce_field(); ?>

											<?php wp_referer_field(); ?>

											<div class="col-lg-12 col-md-12 col-sm-12 form-group form-required">

												<label for="Order_Number" title="The number that visitors will search to find the order."><?php _e("Order Number", 'order-tracking') ?> <span class="required">*</span></label>

												<input type="text" name="Order_Number" id="Order_Number" />

											</div>

											<div class="col-lg-12 col-md-12 col-sm-12 form-group">

												<label for="Order_Name" title="The name of the order your users will see"><?php _e("Order Name", 'order-tracking') ?></label>

												<input name="Order_Name" id="Order_Name" type="text" value="" size="60" />

											</div>

											<div class="col-lg-12 col-md-12 col-sm-12 form-group">

												<label for="Order_Status" title="The status that visitors will see if they enter the order number"><?php _e("Order Status", 'order-tracking') ?></label>

												<select name="Order_Status" id="Order_Status" />

												<?php if (!is_array($Statuses_Array)) {$Statuses_Array = array();}

												foreach ($Statuses_Array as $sts => $Status_Array_Item) { ?>

													<option value='<?php echo $sts; ?>'<?php echo $Status_Array_Item['Status'] == 'Scanned' ? 'selected': ''; ?>><?php echo $Status_Array_Item['Status']; ?></option>

												<?php } ?>

												</select>

											</div>

											<div class="col-lg-12 col-md-12 col-sm-12 form-group">

												<label for="Order_Notes_Public" title="The notes that visitors will see if they enter the order number, and you've included 'Notes' on the options page."><?php _e("Order Notes", 'order-tracking') ?></label>

												<input type="text" name="Order_Notes_Public" id="Order_Notes_Public" />

											</div>

										</div>

										<div class="modal-footer">

											<button class="btn btn-primary" type="button" id="create-order">Tạo đơn hàng</button>

										</div>

									</div>

								</div>

							</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

</footer>

<!-- #colophon -->

</div>

<?php get_footer(); ?>

