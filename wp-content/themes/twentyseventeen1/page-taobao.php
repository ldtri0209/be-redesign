<?php



/*



Template Name: New Tao Bao



*/



get_header(); ?>



<?php


$admin_email = get_option('admin_email') ? get_option('admin_email') : 'contact@logistic.com';

$display = wp_get_current_user()->user_firstname . " " . wp_get_current_user()->user_lastname;

?>







<div class="clear-fix"></div>

<div class="dctrans-taobao-container">

	<div class="head-banner">

		<div class="container banner-container">

			<div class="title"><?php _e('Hỗ trợ nhập hàng trực tiếp từ Taobao', 'dctrans_translate')?></div>

			<div class="slogan col-md-6 col-xs-12"><?php _e('DCTrans cung cấp dịch vụ mua hàng trực tiếp từ Taobao uy tín đảm bảo và không mất bất kỳ khoảng phí trung gian nào. Chúng tôi cam kết hỗ trợ tối đa trong quá trình mua hàng và vận chuyển hàng!', 'dctrans_translate')?>
			<br/>
			<?php _e('Với nhiều năm kinh nghiệm trong ngành và đội ngũ nhân viên giàu kinh nghiệm, chúng tôi mong muốn mang đến chất lượng dịch vụ tốt nhất cho bạn và những người thân.', 'dctrans_translate')?>
			</div>

		</div>

	</div>



	<div class="guide-steps">

		<div class="container guide-inside">

			<!--<div class="col-md-12 caption">Mua hàng Taobao nhanh chóng !</div>-->



			<div class="col-md-4 step-col">

				<ul class="step-ul">

					<li class="image"><img src="<?php echo  get_template_directory_uri() ?>/images/taobao-step1.png"></li>

					<li class="title"><?php _e('Bước 1', 'dctrans_translate')?>:<?php _e(' Mua hàng taobao', 'dctrans_translate')?></li>
					<li class="description">
						<?php _e('Sở hữu ngay tài khoản taobao mua hàng Quảng Châu và các tỉnh khác ở TQ trực tiếp trên taobao/1688/tmall Không đặt hàng qua trung gian, không mất phí dịch vụ. Được hướng dẫn khách cách xem hàng, mua hàng và chốt đơn hàng nhanh chóng.', 'dctrans_translate')?>
					</li>

				</ul>

			</div>



			<div class="col-md-4 step-col">

				<ul class="step-ul">

					<li class="image"><img src="<?php echo  get_template_directory_uri() ?>/images/taobao-step3.png"></li>

					<li class="title"><?php _e('Bước 2', 'dctrans_translate')?>:<?php _e(' Thanh toán', 'dctrans_translate')?></li>
					<li class="description">
						<?php _e('Khách thanh toán số tiền mua hàng kèm phí chuyển đổi tiền tệ bằng cách chuyển khoản cho DCtrans. DCTrans hỗ trợ thanh toán hộ tiền hàng thông qua cổng alipay hoàn toàn miễn phí. Khách hàng được hướng dẫn cách theo dõi đơn hàng của mình trên taobao/1688', 'dctrans_translate')?>
					</li>

				</ul>

			</div>



			<div class="col-md-4 step-col">

				<ul class="step-ul">

					<li class="image"><img src="<?php echo  get_template_directory_uri() ?>/images/taobao-step2.png"></li>

					<li class="title"><?php _e('Bước 3', 'dctrans_translate')?>:<?php _e(' Nhận hàng', 'dctrans_translate')?></li>
					<li class="description">
						<?php _e('Tại kho Quảng Châu, sau khi nhận được hàng, DCtrans tiến hành đóng bao/gỗ và chuyển về Việt Nam. Thời gian nhận hàng về HN từ 2 - 3 ngày, HCM từ 5 - 6 ngày. Khách hàng thanh toán phí vận chuyển sau khi nhận được hàng.', 'dctrans_translate')?>
					</li>

				</ul>

			</div>





		</div>

	</div>





	<!--Our Process-->

	<div class="our-process">

		<div class="container process-inside">

			<div class="col-md-6 left-process">

				<ul class="left-process-ul">

					<li class="caption"><?php _e('Quy trình của chúng tôi', 'dctrans_translate')?></li>
					<li class="step tb-step1"><span class="embed"><?php _e('Bước 1', 'dctrans_translate')?>:</span> <?php _e('Mua hàng trực tiếp Taobao, Dctrans thanh toán hộ', 'dctrans_translate')?></li>
					<li class="step tb-step2"><span class="embed"><?php _e('Bước 2', 'dctrans_translate')?>:</span> <?php _e('Nhà cung cấp giao hàng đến kho Dctrans tại Quảng Châu', 'dctrans_translate')?> </li>
					<li class="step tb-step3"><span class="embed"><?php _e('Bước 3', 'dctrans_translate')?>:</span> <?php _e('Kiểm tra, đóng gói và cân hàng hoá', 'dctrans_translate')?></li>
					<li class="step tb-step4"><span class="embed"><?php _e('Bước 4', 'dctrans_translate')?>:</span> <?php _e('Vận chuyển hàng về Việt Nam', 'dctrans_translate')?></li>
					<li class="step tb-step5"><span class="embed"><?php _e('Bước 5', 'dctrans_translate')?>:</span> <?php _e('Giao hàng cho khách', 'dctrans_translate')?></li>

				</ul>

			</div>

			<div class="col-md-6 right-process">

				<iframe width="560" height="400" src="https://www.youtube.com/embed/GyCPtJWIe30" frameborder="0" allowfullscreen></iframe>

			</div>

		</div>

	</div>

	<div class="clear-fix"></div>


	<!--Price Table-->
	<div class="taobao-price-table price-1">
		<div class="container price-table-inside">
			<div class="caption"><?php _e('BẢNG GIÁ VẬN CHUYỂN ĐƯỜNG BỘ', 'dctrans_translate')?>
		<br/> <span class="sub"><?php _e('Áp dụng từ ngày 05 tháng 08 năm 2017', 'dctrans_translate')?></span></div>

			<div class="col-md-12">
				<div class="headline"><?php _e('1. Bảng giá chung', 'dctrans_translate')?></div>
				<div class="notice">
					<span class="embed"><?php _e('Lưu ý 1', 'dctrans_translate')?>:</span>
					<?php _e('Hà Nội (2-3 ngày)(Tại HN phí trả hàng từ 100.000đ - 120.000)', 'dctrans_translate')?>
				</div>
				<div class="notice">
					<span class="embed"><?php _e('Lưu ý 2', 'dctrans_translate')?>:</span>
					<?php _e('Đà Nẵng, Huế, Tp HCM (4-5ngày)(HCM Giao hàng miễn phí cự ly 10km từ sân bay Tân Sơn Nhất)', 'dctrans_translate')?>
				</div>
				<table class="price-tb">
					<tr class="tb-head">
						<td><?php _e('Mức', 'dctrans_translate')?></td>
						<td><?php _e('Trọng lượng (kg/bao)', 'dctrans_translate')?></td>
						<td><?php _e('Hà Nội', 'dctrans_translate')?></td>
						<td><?php _e('Đà Nẵng, Huế, Tp HCM', 'dctrans_translate')?></td>
						<td><?php _e('Các phí khác', 'dctrans_translate')?></td>
					</tr>
					<tr>
						<td>1</td>
						<td>01 kg  - 09 kg </td>
						<td>32,000/kg</td>
						<td>64.000/kg</td>
						<td>#</td>
					</tr>
					<tr>
						<td>2</td>
						<td>10kg - 19 kg</td>
						<td>27.000/kg</td>
						<td>54.000/kg</td>
						<td>#</td>
					</tr>
					<tr>
						<td>3</td>
						<td>20 kg  - 29 kg</td>
						<td>24.000kg</td>
						<td>47.000/kg</td>
						<td>#</td>
					</tr>
					<tr>
						<td>4</td>
						<td>30 kg - 39kg</td>
						<td>22.000/kg</td>
						<td>42.000/kg</td>
						<td>#</td>
					</tr>
					<tr>
						<td>5</td>
						<td>40 kg  - 49 kg</td>
						<td>21.000/kg</td>
						<td>39.000/kg</td>
						<td>#</td>
					</tr>
					<tr class="last">
						<td>6</td>
						<td><?php _e('trên 50kg', 'dctrans_translate')?></td>
						<td>20.000/kg</td>
						<td>36.000/kg</td>
						<td>#</td>
					</tr>
				</table>
			</div>

			<div class="col-md-12">
				<div class="headline"><?php _e('2. Đối với mặt hàng vải', 'dctrans_translate')?></div>
				<table class="price-tb">
					<tr class="tb-head">
						<td><?php _e('Địa điểm  (Thời gian nhận hàng)', 'dctrans_translate')?></td>
						<td><?php _e('Hà Nội (3-4 ngày)', 'dctrans_translate')?></td>
						<td><?php _e('Đà Nẵng, Huế, Tp HCM (8-9 ngày)', 'dctrans_translate')?></td>
						<td><?php _e('Ghi chú', 'dctrans_translate')?></td>
					</tr>
					<tr class="last">
						<td><?php _e('Đơn giá (vnđ/kg)', 'dctrans_translate')?></td>
						<td>12.000đ/kg</td>
						<td>24.000/kg </td>
						<td><?php _e('Tối thiểu 500kg', 'dctrans_translate')?></td>
					</tr>
				</table>
			</div>

		</div>
	</div>



	<!--Price Table-->
	<div class="taobao-price-table price-2">
		<div class="container price-table-inside">
			<div class="caption"><?php _e('BẢNG GIÁ VẬN CHUYỂN ĐƯỜNG BIỂN ', 'dctrans_translate')?>
		<br/> <span class="sub"><?php _e('Áp dụng từ ngày 05 tháng 08 năm 2017', 'dctrans_translate')?></span></div>

			<div class="col-md-6">
				<div class="headline"><?php _e('1. Vận tải biển đến Hà Nội', 'dctrans_translate')?></div>
				<div class="notice">
					<span class="embed"><?php _e('Lưu ý', 'dctrans_translate')?>:</span>
					<?php _e('Gía nhận hàng tại ga Giáp Bát-Hà Nội，các loại hàng như vải tấm, gạch hoặc các mặt hàng liên 
	quan đến xây dựng sẽ phát sinh thêm phí kiểm định', 'dctrans_translate')?>
				</div>
				<div class="notice">
					<span class="embed"><?php _e('Ghi chú', 'dctrans_translate')?>:</span>
					<?php _e('Trọng lượng 1m3 hàng dưới 280kg', 'dctrans_translate')?>
				</div>
				<table class="price-tb">
					<tr class="tb-head">
						<td><?php _e('Loại', 'dctrans_translate')?></td>
						<td><?php _e('Hàng thông thường', 'dctrans_translate')?></td>
						<td><?php _e('Hàng vải các loại', 'dctrans_translate')?></td>
						<td><?php _e('Hàng điện tử', 'dctrans_translate')?></td>
					</tr>
					<tr>
						<td><?php _e('Hàng cồng kềnh (/m3)', 'dctrans_translate')?></td>
						<td>3.200.000đ/m3</td>
						<td>3.500.000đ/m3</td>
						<td>4.750.000đ/m3</td>
					</tr>
					<tr class="last">
						<td><?php _e('HÀNG NẶNG(/KG)', 'dctrans_translate')?></td>
						<td>11.500đ/kg</td>
						<td>12.500đ/kg</td>
						<td>17.000đ/kg</td>
					</tr>
				</table>
			</div>

			<div class="col-md-6">
				<div class="headline"><?php _e('2. BẢNG GIÁ VẬN TẢI BIỂN ĐẾN HỒ CHÍ MINH', 'dctrans_translate')?></div>
				<div class="notice">
					<span class="embed"><?php _e('Lưu ý', 'dctrans_translate')?>:</span>
					<?php _e('Gía chưa bao gồm phí chuyển tải xe tải tại TPHCM，các loại hàng như vải tấm,
	gạch hoặc các mặt hàng liên quan đến xây dựng sẽ phát sinh thêm phí kiểm định. ', 'dctrans_translate')?>
				</div>
				<div class="notice">
					<span class="embed"><?php _e('Ghi chú', 'dctrans_translate')?>:</span>
					<?php _e('Trọng lượng 1m3 hàng dưới 280kg', 'dctrans_translate')?>
				</div>
				<table class="price-tb">
					<tr class="tb-head">
						<td><?php _e('Loại', 'dctrans_translate')?></td>
						<td><?php _e('Hàng thông thường', 'dctrans_translate')?></td>
						<td><?php _e('Hàng vải các loại', 'dctrans_translate')?></td>
						<td><?php _e('Hàng điện tử', 'dctrans_translate')?></td>
					</tr>
					<tr>
						<td><?php _e('Hàng cồng kềnh (/m3)', 'dctrans_translate')?></td>
						<td>2.800.000đ/m3</td>
						<td>3.080.000đ/m3</td>
						<td>3.800.000đ/m3</td>
					</tr>
					<tr class="last">
						<td><?php _e('HÀNG NẶNG(/KG)', 'dctrans_translate')?></td>
						<td>10.000đ/kg</td>
						<td>11.000đ/kg</td>
						<td>14.000đ/kg</td>
					</tr>
				</table>
			</div>

		</div>
	</div>





	<!--Call to action-->

	<div class="call-reg-container">
		<div class="container separator"></div>
		<div class="title">
			<?php _e('Để sử dụng dịch vụ DCtrans! Đăng ký ngay ', 'dctrans_translate')?>
		</div>

		<div class="description">
			<?php _e('Là thành viên của Dctrans, bạn có thể đặt hàng Online, tư vấn 24/7 và sử dụng những dịch vụ tốt nhất từ Dctrans ', 'dctrans_translate')?>
		</div>

		<div class="reg-line">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>login" class="reg-btn dctrans-btn"><?php _e('Đăng ký', 'dctrans_translate')?></a>
		</div>

	</div>



</div>

</div>

  <div class="clear-fix"></div>

  <div class="shipping-calculator container"><!--////// Shipping Calculator : Writen by : Huy Truong August 12, 2017 -->

    <div class="col-md-6 shipping-left">

      <ul>

        <li class="steps"><img src="<?php echo  get_template_directory_uri() ?>/images/steps.png" /></li>

        <li class="title"><?php _e('Tạo đơn hàng nhanh chóng chỉ với hai bước', 'dctrans_translate')?></li>

        <li class="step-des"><span class="bold"><?php _e('Bước 1', 'dctrans_translate')?>:</span> <?php _e('Lựa chọn dịch vụ vận chuyển đường biển hoặc đường bộ, điền các thông số bắt buộc', 'dctrans_translate')?></li>

        <li class="step-des"><span class="bold"><?php _e('Bước 2', 'dctrans_translate')?>:</span> <?php _e('Kiểm tra kết quả tính toán từ hệ thống và đặt đơn hàng', 'dctrans_translate')?></li>

      </ul>

    </div>

    <div class="col-md-6 shipping-right">

      <div class="calculate-table">

      

        <!-- Nav tabs -->

        <ul class="nav nav-tabs" role="tablist">

          <li role="presentation" class="active">

            <a href="#home" aria-controls="home" role="tab" data-toggle="tab" class="tab-title">

              <i class="fa fa-truck" aria-hidden="true"></i> <?php _e('Đường bộ', 'dctrans_translate')?></a>

          </li>

          <li role="presentation">

            <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" class="tab-title">

              <i class="fa fa-archive" aria-hidden="true"></i> <?php _e('Đường biển', 'dctrans_translate')?></a>

          </li>

        </ul>



        <!-- Tab panes -->

        <div class="tab-content">

          <div role="tabpanel" class="tab-pane active cal-panel" id="home">

              <form action="" method="" id="frm-track-by-land">

            <ul class="tab-cal-ul">

              <li class="cal-txt">

                <label><?php _e('Lựa chọn địa điểm', 'dctrans_translate')?></label>

                <select class="form-control location-select"  name="location">

                  <option value="hcm"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hồ Chí Minh', 'dctrans_translate')?></option>

                  <option value="hn"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hà Nội', 'dctrans_translate')?></option>

                </select>

              </li>

              <li class="cal-txt">

                <label><?php _e('Trọng lượng hàng (kg)', 'dctrans_translate')?></label>

                <input type="text" class="form-control" name="p-weight" placeholder="nhập trọng lượng">

              </li>

              <li class="cal-txt">

                <label><?php _e('Dịch vụ thêm', 'dctrans_translate')?></label>

                <select class="form-control location-select" name="dong_goi"  >

                  <option value=""><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Lựa chọn dịch vụ thêm', 'dctrans_translate')?></option>

                 

                  <option value="khong_dong"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Không đóng gói', 'dctrans_translate')?></option>

                  <option value="dong_bao"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Đóng bao', 'dctrans_translate')?></option>

                  <option value="thung_go"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Đóng thùng gỗ', 'dctrans_translate')?></option>

                </select>

              </li>

              <li class="cal-btn">

              <input type="hidden" id ="action" name="action" value="by_land" />

                <button class="btn btn-default dctrans-btn"><?php _e('Ước tính ngay', 'dctrans_translate')?></button>

              </li>

            </ul>

            </form>

            <div id="div_detail-by-land"></div>

          </div>

          <div role="tabpanel" class="tab-pane cal-panel" id="profile">

          <form action="" method="" id="frm-track-by-sea">

            <ul class="tab-cal-ul">

              <li class="cal-txt">

                <label><?php _e('Lựa chọn địa điểm', 'dctrans_translate')?></label>

                <select class="form-control location-select" name="location">

                  <option value="hcm"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hồ Chí Minh', 'dctrans_translate')?></option>

                  <option value="hn"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hà Nội', 'dctrans_translate')?></option>

                </select>

              </li>

              <li class="cal-txt">

                <div class="col-md-6 segment-left">

                  <label><?php _e('Trọng lượng hàng (kg)', 'dctrans_translate')?></label>

                  <input type="text" class="form-control"  name="p-weight" placeholder="<?php _e('nhập trọng lượng hàng', 'dctrans_translate')?>">

                </div>

                <div class="col-md-6 segment-right">

                  <label><?php _e('Thể tích hàng hoá (m3)', 'dctrans_translate')?></label>

                  <input type="text" class="form-control"    name="category_thetich" placeholder="<?php _e('nhập thể tích hàng', 'dctrans_translate')?>">

                </div>

                <div class="clear-fix"></divL>

              </li>

              <li class="cal-txt">

                <label><?php _e('Loại hàng hoá', 'dctrans_translate')?></label>

                <select class="form-control location-select" name="category_hang" id="mathang">

                  <option value="thongthuong"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hàng thông thường', 'dctrans_translate')?></option>

                  <option value="vai"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Các loại vải', 'dctrans_translate')?></option>

                  <option value="dientu"><img src="<?php echo  get_template_directory_uri() ?>/images/hcm.png" /> <?php _e('Hàng điện tử', 'dctrans_translate')?></option>

                </select>

              </li>



              <li class="cal-btn">

                <input type="hidden" id ="action" name="action" value="by_sea" />

                <button class="btn btn-default dctrans-btn"><?php _e('Ước tính ngay', 'dctrans_translate')?></button>

              </li>

            </ul>



          </form>

          <div id="div_detail-by-sea"></div>



          </div>

        </div>

      </div>



    </div>

  </div>

  <!--///////// 

  //////////// End : Shipping Calculator ///////////////

  ////////////-->



<?php get_footer();

