<?php
/**
 * Additional features to allow styling of the templates
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function twentyseventeen_body_classes( $classes ) {
	// Add class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Add class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Add class if we're viewing the Customizer for easier styling of theme options.
	if ( is_customize_preview() ) {
		$classes[] = 'twentyseventeen-customizer';
	}

	// Add class on front page.
	if ( is_front_page() && 'posts' !== get_option( 'show_on_front' ) ) {
		$classes[] = 'twentyseventeen-front-page';
	}

	// Add a class if there is a custom header.
	if ( has_header_image() ) {
		$classes[] = 'has-header-image';
	}

	// Add class if sidebar is used.
	if ( is_active_sidebar( 'sidebar-1' ) && ! is_page() ) {
		$classes[] = 'has-sidebar';
	}

	// Add class for one or two column page layouts.
	if ( is_page() || is_archive() ) {
		if ( 'one-column' === get_theme_mod( 'page_layout' ) ) {
			$classes[] = 'page-one-column';
		} else {
			$classes[] = 'page-two-column';
		}
	}

	// Add class if the site title and tagline is hidden.
	if ( 'blank' === get_header_textcolor() ) {
		$classes[] = 'title-tagline-hidden';
	}

	// Get the colorscheme or the default if there isn't one.
	$colors = twentyseventeen_sanitize_colorscheme( get_theme_mod( 'colorscheme', 'light' ) );
	$classes[] = 'colors-' . $colors;

	return $classes;
}
add_filter( 'body_class', 'twentyseventeen_body_classes' );

/**
 * Count our number of active panels.
 *
 * Primarily used to see if we have any panels active, duh.
 */
function twentyseventeen_panel_count() {

	$panel_count = 0;

	/**
	 * Filter number of front page sections in Twenty Seventeen.
	 *
	 * @since Twenty Seventeen 1.0
	 *
	 * @param int $num_sections Number of front page sections.
	 */
	$num_sections = apply_filters( 'twentyseventeen_front_page_sections', 4 );

	// Create a setting and control for each of the sections available in the theme.
	for ( $i = 1; $i < ( 1 + $num_sections ); $i++ ) {
		if ( get_theme_mod( 'panel_' . $i ) ) {
			$panel_count++;
		}
	}

	return $panel_count;
}

/**
 * Checks to see if we're on the homepage or not.
 */
function twentyseventeen_is_frontpage() {
	return ( is_front_page() && ! is_home() );
}

add_action( 'wp_enqueue_scripts', 'consulting_child_enqueue_parent_styles' );

function consulting_child_enqueue_parent_styles() {

	
	
	wp_register_style( 'custom-style', get_stylesheet_directory_uri() . '/custom.css', 'all' );
    wp_enqueue_style( 'custom-style' );
	
//  wp_register_style( 'scroll-style1', get_stylesheet_directory_uri() . '/css/jquery.mCustomScrollbar.css', 'all' );
 //   wp_enqueue_style( 'scroll-style1' );

}

// bai viet lien quan
function get_samepost_category($post_id) {
    $categories = get_the_category($post_id);
    if ($categories):
        $category_ids = array();
        foreach($categories as $individual_category):
          $category_ids[] = $individual_category->term_id;
          $args=array(
                'category__in' => $category_ids,
                'post__not_in' => array($post_id),
                'showposts'=>3,
                'ignore_sticky_posts'=>1);
          $my_query = new wp_query($args);
        endforeach;
        if( $my_query->have_posts() ):
           if( is_single() ):?>
             <div class="title-related"><?php echo esc_html__( 'Related articles', 'inttheme' )?></div>

             <div class="content">
			<div class="row">
            
               <?php while ($my_query->have_posts()):
                     $my_query->the_post();?>

                     <div class="item  col-md-4 col-sm-6 col-xs-6">
						<div class="box-item">
						<div class="img-news">
						<a href="<?php the_permalink(); ?>"><?php echo (has_post_thumbnail()) ? get_the_post_thumbnail($post_id, 'recent-size',array( 'class' => 'img-responsive' )) : ''; ?>	</a>
						</div>
						<span class="news-date"><?php echo get_the_date(); ?></span>
						
						<div class="info-news">
							<a href="<?php the_permalink(); ?>"><?php echo wp_trim_words( get_the_title(), 12) ; ?></a>
						</div>
						<p class="line-more"></p>
						<p class="read-more"><a href="<?php the_permalink(); ?>">Read more</a></p>
						</div>
					</div>

               <?php endwhile; ?>
             
             </div>
             </div>
 <?php  endif; endif; endif; wp_reset_query(); }

 //menu
 function my_recent_posts_shortcode($atts){
  
 
                               
  $html .='';
  
return  $html ;  

}
add_shortcode('recent_posts', 'my_recent_posts_shortcode');


add_action( 'wp_ajax_by_land', 'ajax_by_land_func' );
add_action( 'wp_ajax_nopriv_by_land', 'ajax_by_land_func' );
function ajax_by_land_func() {
	$data = $_POST;
	
	$action = $data['action'];
	$location = $data['location'];
	$trongluong = $data['p-weight'];
	$dong_goi  = $data['dong_goi'];
	$category_hang  = $data['category_hang'];
	$the_tich  = $data['ip-thetich'];

	
	if($dong_goi=="khong_dong" ||$dong_goi=='' ){
		$tiendg = 0;
		$namedg = "Không đóng";
	}
	else if($dong_goi=="dong_bao"){
		$tiendg = 187000;
		$namedg = "Đóng bao";
	}
	else if($dong_goi=="thung_go"){
		$tiendg = 340000;
		$namedg = "Đóng thùng gỗ";
	}
	if($trongluong >0 && $trongluong < 10){
		$tienhn = 30000 ;
		$tienhcm = 60000 ;
	}
	else if ($trongluong >10 && $trongluong < 20) {
			$tienhn = 25000 ;
			$tienhcm = 52000 ;
		}
	else if ($trongluong>20 && $trongluong >30) {
			$tienhn = 22000;
			$tienhcm = 43000 ;
		}
	else if ($trongluong>30 && $trongluong <40) {
			$tienhn = 20000;
			$tienhcm = 38000 ;
		}
	else if ($trongluong>40 && $trongluong <50) {
			$tienhn = 19000;
			$tienhcm = 35000 ;
		}
	else if ($trongluong>50 ) {
			$tienhn = 18000;
			$tienhcm = 32000 ;
		}
		
	if($location == 'hn'){
		$tong_tien = $trongluong * $tienhn + $tiendg  ;

	}else if($location =='hcm'){
		$tong_tien = $trongluong * $tienhcm + $tiendg;
	}
	$total = number_format($tong_tien,0,",",".");
	echo '<div class="div_detail_wp">';
	echo '<p><label>Vận chuyển: </label>Đường bộ</p>';
	echo '<p><label>Trọng lượng: </label>' .$trongluong .' kg</p>';
	echo '<p><label>Đóng gói: </label>' . $namedg .'</p>';
	echo '<span class="line-more2"></span>';
	echo '<p class="total_pirce"><label>TỒNG TIỀN:</label>'.number_format($tong_tien,0,",",".").' Đ<p>' ;
	echo '</div>';
	echo '<div class ="box_reoder" ><a href="'.esc_url( home_url( '/' ) ).'"  class="btn_reoder">Tính lại</a> </div>';
	if($total >0){
		echo '<div class ="box_oder" ><a href="'.esc_url( home_url( '/' ) ).'get-order/?mt='.$action.'&lc='.$location.'&cat='.$category_hang.'&tg='.$trongluong.'&dg='.$dong_goi.'&total='.$total.'"  class="btn_oder">Đặt hàng</a></div>';
	}
	
	die();
}

add_action( 'wp_ajax_by_sea', 'ajax_by_sea_func' );
add_action( 'wp_ajax_nopriv_by_sea', 'ajax_by_sea_func' );
function ajax_by_sea_func() {
	$data = $_POST;
	

	$location = $data['location'];
	$trongluong = $data['p-weight'];
	$category_hang  = $data['category_hang'];
	$the_tich  = $data['category_thetich'];
	$action = $data['action'];
	
	if($category_hang=="thongthuong" ){
		
		$namedg = "Hàng thông thường";
		$tienhn = 3500000 ;
		$tienhcm = 2800000 ;
		$tientghn = 12500 ;
		$tientghcm = 10000 ;
	}
	else if($category_hang=="vai"){
		
		$namedg = "Hàng các loại vải";
		$tienhn = 3850000 ;
		$tienhcm = 3080000 ;
		$tientghn = 13750 ;
		$tientghcm = 11000 ;
	}
	else if($category_hang=="dientu"){
		
		$namedg = "Hàng điện tử";
		$tienhn = 4900000 ;
		$tienhcm = 3920000 ;
		$tientghn = 17500 ;
		$tientghcm = 14000 ;
	}
	
	if($location == 'hn'){
		if($the_tich >0){
			$tong_tien = $tienhn * $the_tich ;
		
		}
		else if($trongluong >0){
		$tong_tien = ($trongluong * $tientghn)  ;		
		}
		

	}else if($location =='hcm'){
		if($the_tich >0){
			$tong_tien = $tienhcm * $the_tich ;
		
		}
		else if($trongluong >0){
		$tong_tien = ($trongluong * $tientghcm)  ;		
		}
		$total = number_format($tong_tien,0,",",".");
	//	$tong_tien = ($trongluong * $tientghcm) + ($tienhcm * $the_tich) ;
	}
	echo '<div class="div_detail_wp">';
	echo '<p><label>Vận chuyển: </label>Đường Biển</p>';

	echo '<p><label>Trọng lượng: </label>' .$trongluong .' kg</p>';
	echo '<p><label>Thể tích: </label>' .$the_tich .' m3</p>';
	echo '<p><label>Danh mục: </label>' . $namedg .'</p>';
	echo '<span class="line-more2"></span>';
	echo '<p class="total_pirce"><label>TỒNG TIỀN:</label>'.number_format($tong_tien,0,",",".").' Đ<p>' ;
	echo '</div>';
	echo '<div class ="box_reoder" ><a href="'.esc_url( home_url( '/' ) ).'"  class="btn_reoder">Tính lại</a> </div>';
	if($total >0){
	echo '<div class ="box_oder" ><a href="'.esc_url( home_url( '/' ) ).'get-order/?mt='.$action.'&lc='.$location.'&cat='.$category_hang.'&tg='.$trongluong.'&tt='.$the_tich.'&total='.$total.'"  class="btn_oder">Đặt hàng</a></div>';
	}
	

	die();
}


//tỉ gia

add_shortcode( 'tigia', 'ti_gia_func' );
function ti_gia_func($atts){
	$atts = shortcode_atts( array(
		'name' => 'Nhập số tiền',
		'phi'	=>50,
		'ti_gia'	=>3350,
		'class' =>''
		
	), $atts, 'tigia' );
	$html = '
	<div class ="content-pfs-loan-repayment '. $atts["class"] .'">
	<form action="" method="post" id="subscriple_frm" class="sub-form " >
	<p><label>'. $atts["name"] .': </label>
	<div class="input-group" style="margin-bottom:20px"> <span class="input-group-addon font-blue" id="convCode1">¥</span> <input type="text" name="so_tien" class=" so_tien" style ="width:200px"required placeholder=""></div>
	<input type="hidden" name="ti_gia" value="'. $atts["ti_gia"] .'" />
	<input type="hidden" name="phi" value="'. $atts["phi"] .'" />
	<input type="hidden" name="action" value="ti_gia" />
	<p><input type="submit" class ="btn_submit" value="Tính"><p>
	
	</form>
	 <div id="div_detail"></div>
	 </div>
	';
	$html .= '
	<script type="text/javascript">
		jQuery("#subscriple_frm").submit(function(ev) {
			jQuery.ajax({
				url: "' . admin_url( 'admin-ajax.php' ) . '",
				data: jQuery("#subscriple_frm").serialize(),
				type: "POST",
				beforeSend: function(){
					jQuery("#loadingitme").show();
				},
				success: function(data) {
					jQuery("#loadingitme").hide();
					console.log(data);
					jQuery("#div_detail").html(data);
					
				}

			});
			return false;
    });  
	</script>
	';
	return $html;
}
add_action( 'wp_ajax_ti_gia', 'ajax_ti_gia_func' );
add_action( 'wp_ajax_nopriv_ti_gia', 'ajax_ti_gia_func' );
function ajax_ti_gia_func() {
$data = $_POST;
$price = str_replace(".", "", $data['so_tien']);
$phi_dich_vu = $data['phi'] *  $data['ti_gia'];
if($data['phi']>0){
$tong_tien = $price + $phi_dich_vu ;
}else{
	$tong_tien = $price *  $data['ti_gia'];

}


echo '<span><b>Số tiền chuyển đổi: '.number_format($tong_tien,0,",",".").' VND</b><span>' ;
die();
}

