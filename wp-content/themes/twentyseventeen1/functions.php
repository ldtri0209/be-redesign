<?php

/**

 * Twenty Seventeen functions and definitions

 *

 * @link https://developer.wordpress.org/themes/basics/theme-functions/

 *

 * @package WordPress

 * @subpackage Twenty_Seventeen

 * @since 1.0

 */



/**

 * Twenty Seventeen only works in WordPress 4.7 or later.

 */

if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {

	require get_template_directory() . '/inc/back-compat.php';

	return;

}



/**

 * Sets up theme defaults and registers support for various WordPress features.

 *

 * Note that this function is hooked into the after_setup_theme hook, which

 * runs before the init hook. The init hook is too late for some features, such

 * as indicating support for post thumbnails.

 */

function twentyseventeen_setup() {

	/*

	 * Make theme available for translation.

	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentyseventeen

	 * If you're building a theme based on Twenty Seventeen, use a find and replace

	 * to change 'twentyseventeen' to the name of your theme in all the template files.

	 */

	load_theme_textdomain( 'twentyseventeen' );



	// Add default posts and comments RSS feed links to head.

	add_theme_support( 'automatic-feed-links' );



	/*

	 * Let WordPress manage the document title.

	 * By adding theme support, we declare that this theme does not use a

	 * hard-coded <title> tag in the document head, and expect WordPress to

	 * provide it for us.

	 */

	add_theme_support( 'title-tag' );



	/*

	 * Enable support for Post Thumbnails on posts and pages.

	 *

	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/

	 */

	add_theme_support( 'post-thumbnails' );



	add_image_size( 'twentyseventeen-featured-image', 2000, 1200, true );



	add_image_size( 'twentyseventeen-thumbnail-avatar', 100, 100, true );



	// Set the default content width.

	$GLOBALS['content_width'] = 525;



	// This theme uses wp_nav_menu() in two locations.

	register_nav_menus( array(

		'top'    => __( 'Top Menu', 'twentyseventeen' ),

		'social' => __( 'Social Links Menu', 'twentyseventeen' ),

	) );



	/*

	 * Switch default core markup for search form, comment form, and comments

	 * to output valid HTML5.

	 */

	add_theme_support( 'html5', array(

		'comment-form',

		'comment-list',

		'gallery',

		'caption',

	) );



	/*

	 * Enable support for Post Formats.

	 *

	 * See: https://codex.wordpress.org/Post_Formats

	 */

	add_theme_support( 'post-formats', array(

		'aside',

		'image',

		'video',

		'quote',

		'link',

		'gallery',

		'audio',

	) );



	// Add theme support for Custom Logo.

	add_theme_support( 'custom-logo', array(

		'width'       => 250,

		'height'      => 250,

		'flex-width'  => true,

	) );



	// Add theme support for selective refresh for widgets.

	add_theme_support( 'customize-selective-refresh-widgets' );



	/*

	 * This theme styles the visual editor to resemble the theme style,

	 * specifically font, colors, and column width.

 	 */

	add_editor_style( array( 'assets/css/editor-style.css', twentyseventeen_fonts_url() ) );



	// Define and register starter content to showcase the theme on new sites.

	$starter_content = array(

		'widgets' => array(

			// Place three core-defined widgets in the sidebar area.

			'sidebar-1' => array(

				'text_business_info',

				'search',

				'text_about',

			),



			// Add the core-defined business info widget to the footer 1 area.

			'sidebar-2' => array(

				'text_business_info',

			),



			// Put two core-defined widgets in the footer 2 area.

			'sidebar-3' => array(

				'text_about',

				'search',

			),

		),



		// Specify the core-defined pages to create and add custom thumbnails to some of them.

		'posts' => array(

			'home',

			'about' => array(

				'thumbnail' => '{{image-sandwich}}',

			),

			'contact' => array(

				'thumbnail' => '{{image-espresso}}',

			),

			'blog' => array(

				'thumbnail' => '{{image-coffee}}',

			),

			'homepage-section' => array(

				'thumbnail' => '{{image-espresso}}',

			),

		),



		// Create the custom image attachments used as post thumbnails for pages.

		'attachments' => array(

			'image-espresso' => array(

				'post_title' => _x( 'Espresso', 'Theme starter content', 'twentyseventeen' ),

				'file' => 'assets/images/espresso.jpg', // URL relative to the template directory.

			),

			'image-sandwich' => array(

				'post_title' => _x( 'Sandwich', 'Theme starter content', 'twentyseventeen' ),

				'file' => 'assets/images/sandwich.jpg',

			),

			'image-coffee' => array(

				'post_title' => _x( 'Coffee', 'Theme starter content', 'twentyseventeen' ),

				'file' => 'assets/images/coffee.jpg',

			),

		),



		// Default to a static front page and assign the front and posts pages.

		'options' => array(

			'show_on_front' => 'page',

			'page_on_front' => '{{home}}',

			'page_for_posts' => '{{blog}}',

		),



		// Set the front page section theme mods to the IDs of the core-registered pages.

		'theme_mods' => array(

			'panel_1' => '{{homepage-section}}',

			'panel_2' => '{{about}}',

			'panel_3' => '{{blog}}',

			'panel_4' => '{{contact}}',

		),



		// Set up nav menus for each of the two areas registered in the theme.

		'nav_menus' => array(

			// Assign a menu to the "top" location.

			'top' => array(

				'name' => __( 'Top Menu', 'twentyseventeen' ),

				'items' => array(

					'link_home', // Note that the core "home" page is actually a link in case a static front page is not used.

					'page_about',

					'page_blog',

					'page_contact',

				),

			),



			// Assign a menu to the "social" location.

			'social' => array(

				'name' => __( 'Social Links Menu', 'twentyseventeen' ),

				'items' => array(

					'link_yelp',

					'link_facebook',

					'link_twitter',

					'link_instagram',

					'link_email',

				),

			),

		),

	);



	/**

	 * Filters Twenty Seventeen array of starter content.

	 *

	 * @since Twenty Seventeen 1.1

	 *

	 * @param array $starter_content Array of starter content.

	 */

	$starter_content = apply_filters( 'twentyseventeen_starter_content', $starter_content );



	add_theme_support( 'starter-content', $starter_content );

}

add_action( 'after_setup_theme', 'twentyseventeen_setup' );

add_image_size( 'recent-size', 360, 176, true ,array('center', 'center' ));

/**

 * Set the content width in pixels, based on the theme's design and stylesheet.

 *

 * Priority 0 to make it available to lower priority callbacks.

 *

 * @global int $content_width

 */

function twentyseventeen_content_width() {



	$content_width = $GLOBALS['content_width'];



	// Get layout.

	$page_layout = get_theme_mod( 'page_layout' );



	// Check if layout is one column.

	if ( 'one-column' === $page_layout ) {

		if ( twentyseventeen_is_frontpage() ) {

			$content_width = 644;

		} elseif ( is_page() ) {

			$content_width = 740;

		}

	}



	// Check if is single post and there is no sidebar.

	if ( is_single() && ! is_active_sidebar( 'sidebar-1' ) ) {

		$content_width = 740;

	}



	/**

	 * Filter Twenty Seventeen content width of the theme.

	 *

	 * @since Twenty Seventeen 1.0

	 *

	 * @param int $content_width Content width in pixels.

	 */

	$GLOBALS['content_width'] = apply_filters( 'twentyseventeen_content_width', $content_width );

}

add_action( 'template_redirect', 'twentyseventeen_content_width', 0 );



/**

 * Register custom fonts.

 */

function twentyseventeen_fonts_url() {

	$fonts_url = '';



	/*

	 * Translators: If there are characters in your language that are not

	 * supported by Libre Franklin, translate this to 'off'. Do not translate

	 * into your own language.

	 */

	$libre_franklin = _x( 'on', 'Libre Franklin font: on or off', 'twentyseventeen' );



	if ( 'off' !== $libre_franklin ) {

		$font_families = array();



		$font_families[] = 'Libre Franklin:300,300i,400,400i,600,600i,800,800i';



		$query_args = array(

			'family' => urlencode( implode( '|', $font_families ) ),

			'subset' => urlencode( 'latin,latin-ext' ),

		);



		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );

	}



	return esc_url_raw( $fonts_url );

}



/**

 * Add preconnect for Google Fonts.

 *

 * @since Twenty Seventeen 1.0

 *

 * @param array  $urls           URLs to print for resource hints.

 * @param string $relation_type  The relation type the URLs are printed.

 * @return array $urls           URLs to print for resource hints.

 */

function twentyseventeen_resource_hints( $urls, $relation_type ) {

	if ( wp_style_is( 'twentyseventeen-fonts', 'queue' ) && 'preconnect' === $relation_type ) {

		$urls[] = array(

			'href' => 'https://fonts.gstatic.com',

			'crossorigin',

		);

	}



	return $urls;

}

add_filter( 'wp_resource_hints', 'twentyseventeen_resource_hints', 10, 2 );



/**

 * Register widget area.

 *

 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar

 */

function twentyseventeen_widgets_init() {

	register_sidebar( array(

		'name'          => __( 'Blog Sidebar', 'twentyseventeen' ),

		'id'            => 'sidebar-1',

		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'twentyseventeen' ),

		'before_widget' => '<section id="%1$s" class="widget %2$s">',

		'after_widget'  => '</section>',

		'before_title'  => '<h2 class="widget-title">',

		'after_title'   => '</h2>',

	) );



	register_sidebar( array(

		'name'          => __( 'Footer 1', 'twentyseventeen' ),

		'id'            => 'sidebar-2',

		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),

		'before_widget' => '<section id="%1$s" class="widget %2$s">',

		'after_widget'  => '</section>',

		'before_title'  => '<h2 class="widget-title">',

		'after_title'   => '</h2>',

	) );



	register_sidebar( array(

		'name'          => __( 'Footer 2', 'twentyseventeen' ),

		'id'            => 'sidebar-3',

		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),

		'before_widget' => '<section id="%1$s" class="widget %2$s">',

		'after_widget'  => '</section>',

		'before_title'  => '<h2 class="widget-title">',

		'after_title'   => '</h2>',

	) );

}

add_action( 'widgets_init', 'twentyseventeen_widgets_init' );



/**

 * Replaces "[...]" (appended to automatically generated excerpts) with ... and

 * a 'Continue reading' link.

 *

 * @since Twenty Seventeen 1.0

 *

 * @param string $link Link to single post/page.

 * @return string 'Continue reading' link prepended with an ellipsis.

 */

function twentyseventeen_excerpt_more( $link ) {

	if ( is_admin() ) {

		return $link;

	}



	$link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',

		esc_url( get_permalink( get_the_ID() ) ),

		/* translators: %s: Name of current post */

		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ), get_the_title( get_the_ID() ) )

	);

	return ' &hellip; ' . $link;

}

add_filter( 'excerpt_more', 'twentyseventeen_excerpt_more' );



/**

 * Handles JavaScript detection.

 *

 * Adds a `js` class to the root `<html>` element when JavaScript is detected.

 *

 * @since Twenty Seventeen 1.0

 */

function twentyseventeen_javascript_detection() {

	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";

}

add_action( 'wp_head', 'twentyseventeen_javascript_detection', 0 );



/**

 * Add a pingback url auto-discovery header for singularly identifiable articles.

 */

function twentyseventeen_pingback_header() {

	if ( is_singular() && pings_open() ) {

		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );

	}

}

add_action( 'wp_head', 'twentyseventeen_pingback_header' );



/**

 * Display custom color CSS.

 */

function twentyseventeen_colors_css_wrap() {

	if ( 'custom' !== get_theme_mod( 'colorscheme' ) && ! is_customize_preview() ) {

		return;

	}



	require_once( get_parent_theme_file_path( '/inc/color-patterns.php' ) );

	$hue = absint( get_theme_mod( 'colorscheme_hue', 250 ) );

?>

	<style type="text/css" id="custom-theme-colors" <?php if ( is_customize_preview() ) { echo 'data-hue="' . $hue . '"'; } ?>>

		<?php echo twentyseventeen_custom_colors_css(); ?>

	</style>

<?php }

add_action( 'wp_head', 'twentyseventeen_colors_css_wrap' );



/**

 * Enqueue scripts and styles.

 */

function twentyseventeen_scripts() {

	// Add custom fonts, used in the main stylesheet.

	wp_enqueue_style( 'twentyseventeen-fonts', twentyseventeen_fonts_url(), array(), null );



	// Theme stylesheet.

	wp_enqueue_style( 'twentyseventeen-style', get_stylesheet_uri() );



	// Load the dark colorscheme.

	if ( 'dark' === get_theme_mod( 'colorscheme', 'light' ) || is_customize_preview() ) {

		wp_enqueue_style( 'twentyseventeen-colors-dark', get_theme_file_uri( '/assets/css/colors-dark.css' ), array( 'twentyseventeen-style' ), '1.0' );

	}



	// Load the Internet Explorer 9 specific stylesheet, to fix display issues in the Customizer.

	if ( is_customize_preview() ) {

		wp_enqueue_style( 'twentyseventeen-ie9', get_theme_file_uri( '/assets/css/ie9.css' ), array( 'twentyseventeen-style' ), '1.0' );

		wp_style_add_data( 'twentyseventeen-ie9', 'conditional', 'IE 9' );

	}



	// Load the Internet Explorer 8 specific stylesheet.

	wp_enqueue_style( 'twentyseventeen-ie8', get_theme_file_uri( '/assets/css/ie8.css' ), array( 'twentyseventeen-style' ), '1.0' );

	wp_style_add_data( 'twentyseventeen-ie8', 'conditional', 'lt IE 9' );



	// Load the html5 shiv.

	wp_enqueue_script( 'html5', get_theme_file_uri( '/assets/js/html5.js' ), array(), '3.7.3' );

	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );



	wp_enqueue_script( 'twentyseventeen-skip-link-focus-fix', get_theme_file_uri( '/assets/js/skip-link-focus-fix.js' ), array(), '1.0', true );



	$twentyseventeen_l10n = array(

		'quote'          => twentyseventeen_get_svg( array( 'icon' => 'quote-right' ) ),

	);



	if ( has_nav_menu( 'top' ) ) {

		wp_enqueue_script( 'twentyseventeen-navigation', get_theme_file_uri( '/assets/js/navigation.js' ), array( 'jquery' ), '1.0', true );

		$twentyseventeen_l10n['expand']         = __( 'Expand child menu', 'twentyseventeen' );

		$twentyseventeen_l10n['collapse']       = __( 'Collapse child menu', 'twentyseventeen' );

		$twentyseventeen_l10n['icon']           = twentyseventeen_get_svg( array( 'icon' => 'angle-down', 'fallback' => true ) );

	}



	wp_enqueue_script( 'twentyseventeen-global', get_theme_file_uri( '/assets/js/global.js' ), array( 'jquery' ), '1.0', true );



	wp_enqueue_script( 'jquery-scrollto', get_theme_file_uri( '/assets/js/jquery.scrollTo.js' ), array( 'jquery' ), '2.1.2', true );



	wp_localize_script( 'twentyseventeen-skip-link-focus-fix', 'twentyseventeenScreenReaderText', $twentyseventeen_l10n );



	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {

		wp_enqueue_script( 'comment-reply' );

	}

}

add_action( 'wp_enqueue_scripts', 'twentyseventeen_scripts' );



/**

 * Add custom image sizes attribute to enhance responsive image functionality

 * for content images.

 *

 * @since Twenty Seventeen 1.0

 *

 * @param string $sizes A source size value for use in a 'sizes' attribute.

 * @param array  $size  Image size. Accepts an array of width and height

 *                      values in pixels (in that order).

 * @return string A source size value for use in a content image 'sizes' attribute.

 */

function twentyseventeen_content_image_sizes_attr( $sizes, $size ) {

	$width = $size[0];



	if ( 740 <= $width ) {

		$sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';

	}



	if ( is_active_sidebar( 'sidebar-1' ) || is_archive() || is_search() || is_home() || is_page() ) {

		if ( ! ( is_page() && 'one-column' === get_theme_mod( 'page_options' ) ) && 767 <= $width ) {

			 $sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';

		}

	}



	return $sizes;

}

add_filter( 'wp_calculate_image_sizes', 'twentyseventeen_content_image_sizes_attr', 10, 2 );



/**

 * Filter the `sizes` value in the header image markup.

 *

 * @since Twenty Seventeen 1.0

 *

 * @param string $html   The HTML image tag markup being filtered.

 * @param object $header The custom header object returned by 'get_custom_header()'.

 * @param array  $attr   Array of the attributes for the image tag.

 * @return string The filtered header image HTML.

 */

function twentyseventeen_header_image_tag( $html, $header, $attr ) {

	if ( isset( $attr['sizes'] ) ) {

		$html = str_replace( $attr['sizes'], '100vw', $html );

	}

	return $html;

}

add_filter( 'get_header_image_tag', 'twentyseventeen_header_image_tag', 10, 3 );



/**

 * Add custom image sizes attribute to enhance responsive image functionality

 * for post thumbnails.

 *

 * @since Twenty Seventeen 1.0

 *

 * @param array $attr       Attributes for the image markup.

 * @param int   $attachment Image attachment ID.

 * @param array $size       Registered image size or flat array of height and width dimensions.

 * @return string A source size value for use in a post thumbnail 'sizes' attribute.

 */

function twentyseventeen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {

	if ( is_archive() || is_search() || is_home() ) {

		$attr['sizes'] = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';

	} else {

		$attr['sizes'] = '100vw';

	}



	return $attr;

}

add_filter( 'wp_get_attachment_image_attributes', 'twentyseventeen_post_thumbnail_sizes_attr', 10, 3 );



/**

 * Use front-page.php when Front page displays is set to a static page.

 *

 * @since Twenty Seventeen 1.0

 *

 * @param string $template front-page.php.

 *

 * @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.

 */

function twentyseventeen_front_page_template( $template ) {

	return is_home() ? '' : $template;

}

add_filter( 'frontpage_template',  'twentyseventeen_front_page_template' );



/**

 * Implement the Custom Header feature.

 */

require get_parent_theme_file_path( '/inc/custom-header.php' );



/**

 * Custom template tags for this theme.

 */

require get_parent_theme_file_path( '/inc/template-tags.php' );



/**

 * Additional features to allow styling of the templates.

 */

require get_parent_theme_file_path( '/inc/template-functions.php' );



/**

 * Customizer additions.

 */

require get_parent_theme_file_path( '/inc/customizer.php' );



/**

 * SVG icons functions and filters.

 */

require get_parent_theme_file_path( '/inc/icon-functions.php' );





//TriLe custom function



add_action( 'wp_enqueue_scripts', 'ajax_custom_order' );

function ajax_custom_order() {

	wp_enqueue_script( 'custom', get_template_directory_uri() . '/assets/js/custom.js',

		array( 'jquery' )

	);

	wp_localize_script( 'custom', 'ajax_object', array(

		'ajax_url' => admin_url( 'admin-ajax.php' )

	));

}



add_action('wp_ajax_nopriv_delete_order_by_id', 'delete_order_by_id');

add_action('wp_ajax_delete_order_by_id', 'delete_order_by_id');

function delete_order_by_id() {



	global $wpdb;

	global $EWD_OTP_orders_table_name, $EWD_OTP_order_statuses_table_name, $EWD_OTP_fields_meta_table_name, $EWD_OTP_OrderUser;



	$Order_ID = $_POST['order_id'];

	$wpdb->delete(

		$EWD_OTP_orders_table_name,

		array('Order_ID' => $Order_ID)

	);

	$wpdb->delete(

		$EWD_OTP_order_statuses_table_name,

		array('Order_ID' => $Order_ID)

	);

	$wpdb->delete(

		$EWD_OTP_fields_meta_table_name,

		array('Order_ID' => $Order_ID)

	);

	$wpdb->delete(

		$EWD_OTP_OrderUser,

		array('order_id' => $Order_ID)

	);



	$update = json_encode([

		'sts' => true,

		'msg' => 'Order has been deleted successfully'

	]);



	echo $update;



	die();

}



add_action('wp_ajax_nopriv_add_order_to_user', 'add_order_to_user');

add_action('wp_ajax_add_order_to_user', 'add_order_to_user');

function add_order_to_user() {

	global $wpdb;

	global $EWD_OTP_OrderUser;

	$code = $_POST['code'];



	$result = Check_Order_In_User($code);



	if(!$result) {



		$update = json_encode([

			'sts' => false,

			'msg' => 'Order <strong>'.$code.'</strong> dose not exist!'

		]);



		echo $update;



		die();

	}



	$wpdb->insert($EWD_OTP_OrderUser, [

			'user_id' => get_current_user_id(),

			'order_id' => $result

		]

	);



	$update = json_encode([

		'sts' => true,

		'msg' => 'Order has been added successfully'

	]);



	echo $update;



	die();

}





add_action('wp_ajax_nopriv_shipped_order', 'shipped_order');

add_action('wp_ajax_shipped_order', 'shipped_order');

function shipped_order() {

	global $wpdb;

	global $EWD_OTP_orders_table_name;

	$order = $_POST['order'];

	$addr = $_POST['add'];



	$wpdb->update($EWD_OTP_orders_table_name, ['Order_Shipping_Info' => $addr], ['Order_Number' => $order]);



	$update = json_encode([

		'sts' => true,

		'msg' => __("Shipping info added successful!", 'order-tracking')

	]);



	echo $update;



	die();

}



add_action('wp_ajax_nopriv_create_order', 'create_order');

add_action('wp_ajax_create_order', 'create_order');

function create_order() {

	global $wpdb, $EWD_OTP_orders_table_name, $EWD_OTP_customers;



	$Order_Email = get_option("EWD_OTP_Order_Email");

	$Timezone = get_option("EWD_OTP_Timezone");

	date_default_timezone_set($Timezone);



	$Statuses_Array = get_option("EWD_OTP_Statuses_Array");

	if (!is_array($Statuses_Array)) {$Statuses_Array = array();}



	$Order_ID = $_POST['Order_ID'];

	$Order_Name = $_POST['Order_Name'];

	$Order_Number = $_POST['Order_Number'];

	$Order_Status = $_POST['Order_Status'];

	if(isset($_POST['Order_Location'])) {$Order_Location = $_POST['Order_Location'];}

	else {$Order_Location = "";}

	$Order_Notes_Public = $_POST['Order_Notes_Public'] ? $_POST['Order_Notes_Public'] : 'Add by supervisor.';

	$Order_Notes_Private = $_POST['Order_Notes_Private'];

	$Order_Shipping_Info = $_POST['Order_Shipping_Info'];

	$Order_Email_Address = $_POST['Order_Email'];

	$Order_Display = $_POST['Order_Display'];

	$Customer_ID = $_POST['Customer_ID'];

	$Sales_Rep_ID = $_POST['Sales_Rep_ID'];

	if(isset($_POST['Order_Payment_Price'])) {$Order_Payment_Price = $_POST['Order_Payment_Price'];}

	else {$Order_Payment_Price = 0;}

	if(isset($_POST['Order_Payment_Completed'])) {$Order_Payment_Completed = $_POST['Order_Payment_Completed'];}

	else {$Order_Payment_Completed = "No";}

	if(isset($_POST['Order_PayPal_Receipt_Number'])) {$Order_PayPal_Receipt_Number = $_POST['Order_PayPal_Receipt_Number'];}

	else {$Order_PayPal_Receipt_Number = "";}

	$Order_Status_Updated = date("Y-m-d H:i:s");

	if ($_POST['action'] != "Add_Order") {

		$Order_Info = $wpdb->get_row($wpdb->prepare("SELECT * FROM $EWD_OTP_orders_table_name WHERE Order_ID=%d", $Order_ID));

		$Current_Status = $Order_Info->Order_Status;

	}



	foreach ($Statuses_Array as $Status_Array_Item) {

		if ($Order_Status == $Status_Array_Item['Status']) {

			if ($Status_Array_Item['Internal'] != "Yes") {

				$Order_External_Status = $Order_Status;

				$Order_Internal_Status = "No";

			}

			else {

				if (isset($Order_Info)) {$Order_External_Status = $Order_Info->Order_External_Status;}

				else {$Order_External_Status = "";}

				$Order_Internal_Status = "Yes";

			}

		}

	}



	if (!isset($error)) {

		// Pass the data to the appropriate function in Update_Admin_Databases.php to create the product

		if ($_POST['isAddNew']) {

			$user_update = Add_EWD_OTP_Order($Order_Name, $Order_Number, $Order_Email_Address, $Order_Status, $Order_External_Status, $Order_Location, $Order_Notes_Public, $Order_Notes_Private, $Order_Display, $Order_Status_Updated, $Customer_ID, $Sales_Rep_ID, $Order_Payment_Price, $Order_Payment_Completed, $Order_PayPal_Receipt_Number, $Order_Internal_Status);

			if (($Order_Email == "Change" or $Order_Email == "Creation") and $Order_Email_Address != "") {EWD_OTP_Send_Email($Order_Email_Address, $Order_Number, $Order_Status, $Order_Notes_Public, $Order_Status_Updated, $Order_Name, "Yes");}

		}

		// Pass the data to the appropriate function in Update_Admin_Databases.php to edit the product

		else {

			$user_update = Edit_EWD_OTP_Order($Order_ID, $Order_Name, $Order_Number, $Order_Email_Address, $Order_Status, $Order_External_Status, $Order_Location, $Order_Notes_Public, $Order_Notes_Private, $Order_Display, $Order_Status_Updated, $Customer_ID, $Sales_Rep_ID, $Order_Payment_Price, $Order_Payment_Completed, $Order_PayPal_Receipt_Number, $Order_Internal_Status, $Order_Shipping_Info);

			if ($Order_Email == "Change" and $Order_Email_Address != "" and $Current_Status != $Order_Status and $Order_Internal_Status != "Yes") {EWD_OTP_Send_Email($Order_Email_Address, $Order_Number, $Order_Status, $Order_Notes_Public, $Order_Status_Updated, $Order_Name);}

		}

		$update = json_encode([

			'sts' => true,

			'msg' =>$user_update

		]);

	}

	// Return any error that might have occurred

	else {

		$update = json_encode([

			'sts' => false,

			'msg' =>$error

		]);

	}



	echo $update;



	die();

}





add_action('wp_ajax_nopriv_confirm_ship_order', 'confirm_ship_order');

add_action('wp_ajax_confirm_ship_order', 'confirm_ship_order');

function confirm_ship_order() {

	global $wpdb;

	global $EWD_OTP_orders_table_name;

	$value = $_POST['value'];

	$order = $_POST['order'];



	$wpdb->update($EWD_OTP_orders_table_name, ['Confirm_Shipped_Order' => $value], ['Order_ID' => $order]);



	$update = json_encode([

		'sts' => true,

		'msg' => __("Order was shipped successful!", 'order-tracking')

	]);



	echo $update;



	die();

}





//add_action('wp_ajax_nopriv_confirm_ship_order', 'sync_tracking_code_from_google_spreadsheet');

// add_action('wp_ajax_sync_tracking_code_from_google_spreadsheet', 'sync_tracking_code_from_google_spreadsheet');

function sync_tracking_code_from_google_spreadsheet() {



    try {

        global $wpdb;

        global $EWD_OTP_orders_table_name;



        $return = [

            'sts' => true,

            'msg' => 'Sync successful!'

        ];



        header('Content-type: application/json');

        $feed = get_option('galogin')['google_sheet_url'];



        if (! $feed) {



            echo json_encode([

                'sts' => FALSE,

                'msg' => 'Please config Google Sheet URL for sync data!'

            ]);



            die();

        }



        $data = $newArray = $keys = [];

		$stsArray = get_option("EWD_OTP_Statuses_Array");

        $stsReceived = $stsArray[0]['Status'];

		$stsScanned = $stsArray[1]['Status'];

		$stsShipped = $stsArray[2]['Status'];

		$stsCompleted = $stsArray[3]['Status'];



        if (($handle = fopen($feed, 'r')) !== FALSE) {

            $i = 0;

            while (($lineArray = fgetcsv($handle, 4000, ',', '"')) !== FALSE) {

                for ($j = 0; $j < count($lineArray); $j++) {

                    $data[$i][$j] = $lineArray[$j];

                }

                $i++;

            }

            fclose($handle);

        }



		$khoGuiTam = array_column($data, 1);

		$khoXuatTam = array_column($data, 12);



		$khoGui = restructure_data($khoGuiTam);

		$khoXuat = restructure_data($khoXuatTam);



        foreach ($data as $key => $value) {



			if ($key < 4) continue;



			if ($value[3] && $value[5]) {



				$rs = check_order_already_exist($value[3]);



				if (! $rs) {

					$id = Add_EWD_OTP_Order('', $value[3], '', $stsReceived, '', '', 'Sync from google sheet.', '', '', '', '', '', '', '', '', '', '', '', TRUE);

					in_array($value[3], $khoGui) && Update_EWD_OTP_Order_Status($id, $stsScanned, '');

					in_array($value[3],$khoXuat) && Update_EWD_OTP_Order_Status($id, $stsShipped, '');

				} else {



					if ($value[3] && in_array($value[3], $khoGui) && $rs->Order_Status == $stsReceived) {

						//Update status to scanned

						Update_EWD_OTP_Order_Status($rs->Order_ID, $stsScanned, '');

						$value[12] && $value[12] == $value[3] && Update_EWD_OTP_Order_Status($rs->Order_ID, $stsShipped, '');



					}



					if ($value[3] && in_array($value[3],$khoXuat) && $rs->Order_Status == $stsScanned) {

						//Update status to Shipped

						Update_EWD_OTP_Order_Status($rs->Order_ID, $stsShipped, '');



					}

				}

			}

        }



        echo json_encode($return);



        die();



    } catch (Exception $e) {



        echo json_encode([

            'sts' => FALSE,

            'msg' => 'Can not connect Google sheet.'

        ]);



        die();

    }



}

// wp_schedule_event(time(), 'daily', 'wp_ajax_sync_tracking_code_from_google_spreadsheet');





add_action('wp_ajax_nopriv_hidden_order_by_id', 'hidden_order_by_id');

add_action('wp_ajax_hidden_order_by_id', 'hidden_order_by_id');

function hidden_order_by_id() {



	global $wpdb;

	global $EWD_OTP_orders_table_name, $EWD_OTP_OrderUser;



	$Order_ID = $_POST['order_id'];



	$wpdb->delete(

		$EWD_OTP_OrderUser,

		array('order_id' => $Order_ID)

	);



	$update = json_encode([

		'sts' => true,

		'msg' => 'Order has been deleted successfully'

	]);



	echo $update;



	die();

}





function check_order_already_exist($order_id) {



	global $wpdb;

	global $EWD_OTP_orders_table_name, $EWD_OTP_OrderUser;



	$sql = "SELECT  Order_ID, Order_Number, Order_Status

			FROM    $EWD_OTP_orders_table_name

			WHERE   Order_Number = %s";



	$rs = $wpdb->get_row($wpdb->prepare($sql, $order_id));



	return $rs;

}



function restructure_data($data) {

	$newData = [];

	foreach ($data as $key => $code) {

		$temp = explode("\n", $code);

		if (count($temp) > 1) {

			unset($data[$key]);

			$newData = array_merge($newData, $temp);

		}

	}



	$return = array_merge($data, $newData);



	return array_filter($return);

}



function parseStatusLanguage($sts=NULL) {



	$vi = [

		'Received' => [

			'Status' => 'Đã nhận đơn hàng',

			'Percentage' => '25',

			'Message' => 'Default',

			'Internal' => 'No'

		],

		'Scanned' => [

			'Status' => 'Đã nhập hàng vào kho',

			'Percentage' => '50',

			'Message' => 'Default',

			'Internal' => 'No'

		],

		'Shipped' => [

			'Status' => 'Đã vận chuyển hàng về VN',

			'Percentage' => '75',

			'Message' => 'Default',

			'Internal' => 'No'

		],

		'Completed' => [

			'Status' => 'Hàng đã đến nơi',

			'Percentage' => '100',

			'Message' => 'Default',

			'Internal' => 'No'

		]

	];



	return $sts ? $vi[$sts]['Status'] : (ICL_LANGUAGE_CODE == 'vi' ? $vi : get_option("EWD_OTP_Statuses_Array"));



}



add_action('admin_init', 'add_currency_option');

function add_currency_option () {

	add_settings_field(

		'currency',

		'Chuyển đổi tiền tệ',

		'currency_display',

		'general'

	);

	add_settings_field(

		'phi_dich_vu',

		'Phí dịch vụ',

		'phi_dich_vu_display',

		'general'

	);

	add_settings_field(

		'token_integrate',

		'Token Integrate',

		'token_integrate_display',

		'general'

	);

	add_settings_field(

		'url_integrate',

		'URL Integrate',

		'url_integrate_display',

		'general'

	);

	add_settings_field(

		'tool_management',

		'Tool Management',

		'tool_management_display',

		'general'

	);



	//register

	register_setting(

		'general',

		'currency'

	);

	register_setting(

		'general',

		'phi_dich_vu'

	);

	register_setting(

		'general',

		'token_integrate'

	);

	register_setting(

		'general',

		'url_integrate'

	);

	register_setting(

		'general',

		'tool_management'

	);



	function currency_display(){

		echo '<span>1 NDT = </span><input type="text" name="currency" id="currency" value="'.get_option('currency').'"><span> VND</span>';

	}

	function phi_dich_vu_display(){

		echo '<input type="text" name="phi_dich_vu" id="phi_dich_vu" value="'.get_option('phi_dich_vu').'">';

	}

	function token_integrate_display(){

		echo '<input type="text" name="token_integrate" id="token_integrate" value="'.get_option('token_integrate').'">';

	}

	function url_integrate_display(){

		echo '<input type="text" name="url_integrate" id="url_integrate" value="'.get_option('url_integrate').'">';

	}

	function tool_management_display(){

		echo '<input type="text" name="tool_management" id="tool_management" value="'.get_option('tool_management').'">';

	}

}

add_action('wp_ajax_nopriv_store_rating_value', 'store_rating_value');
add_action('wp_ajax_store_rating_value', 'store_rating_value');

function store_rating_value() {
	try {
		global $wpdb;
		$email = $_POST['email'];
		$value = $_POST['star'];

		$wpdb->insert('wp_rating', [
			'email' => $email,
			'value' => $value
		]);

		echo 'Ok!';

	} catch (Exception $e) {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	}

	die();

}


 function remove_menus(){  

   remove_menu_page( 'index.php' );                  //Dashboard  
   //remove_menu_page( 'edit.php' );                   //Posts  
   //remove_menu_page( 'upload.php' );                 //Media  
   //remove_menu_page( 'edit.php?post_type=page' );    //Pages  
   //remove_menu_page( 'edit-comments.php' );          //Comments  
    remove_menu_page( 'themes.php' );                 //Appearance  
    remove_menu_page( 'plugins.php' );                //Plugins  
   //remove_menu_page( 'users.php' );                  //Users  
   // remove_menu_page( 'tools.php' );                  //Tools  
   //remove_menu_page( 'options-general.php' );        //Settings   

 }  
 add_action( 'admin_menu', 'remove_menus' );


function pagenavi_paged($q) {
    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
    $q->set('paged', $paged);
}
add_action('pre_get_posts', 'pagenavi_paged');
    
// Replace wpv-pagination shortcode
function wpv_pagenavi($args, $content) {
  global $WP_Views;
  return wp_pagenavi( array('query' => $WP_Views->post_query, 'echo'=>false) );
}
add_shortcode('wpv-pagination', 'wpv_pagenavi');