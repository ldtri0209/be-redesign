/**



 * Created by Mr Le on 6/20/2017.



 */



jQuery.noConflict();







(function($){



    //sticky cta

    $(document).scroll(function() {

        var y = $(this).scrollTop();

        if (y > 150) {

            $('html').addClass('scrolled-stick-menu');

        } else {

            $('html').removeClass('scrolled-stick-menu');

        }

    });



    $(document).on({



        ajaxStart: function() {



            $("body").addClass("loading");



        },



        ajaxStop: function() {



            $("body").removeClass("loading");



        }



    });





    $(document).ready(function() {



        $('.delete-order').click(deleteOrder);



        $('#add-order').click(addOrder);



        $('#update-order').click(updateOrder);



        $('.ship-order').click(prepareShipOrder);



        $('#shipment-order').click(processShipOrder);



        $('#create-order').click(createOrder);



        $('#Confirm_Shipped_Order').change(confirmIsShipped);



        $('#sync-data').click(processSyncData);



        $('.hidden-order').click(removeOrder);



        $( ".cross-btn" ).hide();

        

        $( ".burger-menu" ).hide();



        $( ".hamburger" ).click(function() {

            $( ".burger-menu" ).slideToggle( "slow", function() {



                $( ".hamburger" ).hide();

                $( ".cross-btn" ).show();

            });

        });



        $( ".cross-btn" ).click(function() {

            $( ".burger-menu" ).slideToggle( "slow", function() {



                $( ".cross-btn" ).hide();

                $( ".hamburger" ).show();

            });

        });

        $( ".wpcf7-email" ).change(function() {
            console.log($(this).val())
            $( "#rating-email" ).val($(this).val());
        });


        $.get("http://ipinfo.io", function(response){ 

            var arr = window.location.pathname.split('/');

            if (response.country == 'CN' && arr[1] != 'zh-hant') {
                window.location.replace(window.location.origin+'/zh-hant'+window.location.pathname);
            }

        }, "jsonp"); 



    });





    var createOrder = function() {



        var order = $('#Order_Number').val();



        if (order) {



            jQuery('#createOrderModal').modal('toggle');



            $.ajax({



                url: ajax_object.ajax_url,



                type: 'post',



                data: {



                    action: 'create_order',



                    isAddNew: 1,



                    Order_Number: order,



                    Order_Name: $('#Order_Name').val(),



                    Order_Status: $('#Order_Status').val(),



                    Order_Notes_Public: $('#Order_Notes_Public').val(),



                    Order_Display: 'Yes'



                },



                success: function(res) {



                    var response = JSON.parse(res);



                    var clSts = response.sts ? 'success' : 'err';



                    jQuery.alert({

                        title: 'Successful!',

                        type: 'green',

                        content: 'Create order successful.',

                    });



                    response.sts && setTimeout(function(){ window.location.reload(); }, 1000);



                }







            })



        } else {

            jQuery.alert({

                title: 'Warning!',

                type: 'red',

                content: 'Please input order number!',

            });



        }



    };







    var addOrder = function() {



        var code = $('#tracking-code').val();



        if (code) {

            jQuery.confirm({

                title: 'Are your sure!',

                content: 'Do you want to add order ' + code + ' to your account?',

                type: 'orange',

                typeAnimated: true,

                buttons: {

                    tryAgain: {

                        text: 'Ok',

                        btnClass: 'btn-blue',

                        action: function(){

                            $.ajax({



                                url: ajax_object.ajax_url,



                                type: 'post',



                                data: {



                                    action: 'add_order_to_user',



                                    code: code



                                },



                                success: function(res) {



                                    var response = JSON.parse(res);



                                    var clSts = response.sts ? 'success' : 'err';



                                    $('#notifi').css('display', 'block');



                                    $('#notifi').removeClass('success err').addClass(clSts);



                                    $('#notification').html(response.msg);



                                    response.sts && setTimeout(function(){ window.location.reload(); }, 1000);



                                }



                            });

                        }

                    },

                    close: function () {

                    }

                }

            });

        } else {

            jQuery.alert({

                title: 'Warning!',

                type: 'red',

                content: 'Please input order number!',

            });



        }



    };







    var updateOrder = function() {



        var order = $('#Order_Number').val();





        jQuery.confirm({

            title: 'Are your sure!',

            content: 'Do you want to update order ' + order,

            type: 'orange',

            typeAnimated: true,

            buttons: {

                tryAgain: {

                    text: 'Ok',

                    btnClass: 'btn-blue',

                    action: function(){



                        $.ajax({



                            url: ajax_object.ajax_url,



                            type: 'post',



                            data: {



                                action: 'create_order',



                                isAddNew: 0,



                                Order_Number: order,



                                Order_ID: $('#Order_ID').val(),



                                Order_Name: $('#Order_Name').val(),



                                Order_Status: $('#Order_Status').val(),



                                Order_Notes_Public: $('#Order_Notes_Public').val(),



                                Order_Shipping_Info: $('#Order_Shipping_Info').val(),



                                Order_Display: 'Yes'



                            },



                            success: function(res) {



                                var response = JSON.parse(res);



                                var clSts = response.sts ? 'success' : 'err';



                                $('#notifi').css('display', 'block').removeClass('success err').addClass(clSts);



                                $('#notification').html(response.msg);



                                response.sts && setTimeout(function(){



                                    //window.location.reload();



                                    window.location.replace(window.location.origin + '/orders-management');



                                }, 1000);



                            }







                        })

                    }

                },

                close: function () {

                }

            }

        });



    };







    var deleteOrder = function() {





        var order_id = $(this).attr('order-id'),



            row = $(this).attr('row-index');





        jQuery.confirm({

            title: 'Are your sure!',

            content: 'Do you want to delete this order?',

            type: 'red',

            typeAnimated: true,

            buttons: {

                tryAgain: {

                    text: 'Delete',

                    btnClass: 'btn-red',

                    action: function(){



                        $.ajax({



                            url: ajax_object.ajax_url,



                            type: 'post',



                            data: {



                                action: 'delete_order_by_id',



                                order_id: order_id



                            },



                            success: function(res) {



                                var response = JSON.parse(res);



                                var clSts = response.sts ? 'success' : 'err';



                                $('#notifi').css('display', 'block');



                                $('#notifi').removeClass('success err').addClass(clSts);



                                $('#notifi span').html(response.msg);



                                response.sts && setTimeout(function(){ window.location.reload(); }, 1000);



                            }







                        })

                    }

                },

                close: function () {

                }

            }

        });





    };







    var removeOrder = function() {





        var order_id = $(this).attr('order-id'),



            row = $(this).attr('row-index');





        jQuery.confirm({

            title: 'Are your sure!',

            content: 'Do you want to delete this order?',

            type: 'red',

            typeAnimated: true,

            buttons: {

                tryAgain: {

                    text: 'Delete',

                    btnClass: 'btn-red',

                    action: function(){



                        $.ajax({



                            url: ajax_object.ajax_url,



                            type: 'post',



                            data: {



                                action: 'hidden_order_by_id',



                                order_id: order_id



                            },



                            success: function(res) {



                                var response = JSON.parse(res);



                                var clSts = response.sts ? 'success' : 'err';



                                $('#notifi').css('display', 'block');



                                if (response.sts) $('#order-management')[0].deleteRow(parseInt(row)+1);



                                $('#notifi').removeClass('success err').addClass(clSts);



                                $('#notification').html(response.msg);



                            }







                        })

                    }

                },

                close: function () {

                }

            }

        });





    };







    var prepareShipOrder = function() {



        var order = $(this).attr('order-number');



        $('#titleOrderNum').html(order);



        $('#order-number-hidden').val(order);



    };







    var processShipOrder = function() {



        var order = $('#order-number-hidden').val(),



            addr = $('#addInput').val();



        if (order && addr) {



            jQuery('#shipmentModal').modal('toggle');



            jQuery.confirm({

                title: 'Are your sure!',

                content: 'Do you want to update order ' + order + '?',

                type: 'orange',

                typeAnimated: true,

                buttons: {

                    tryAgain: {

                        text: 'Ok',

                        btnClass: 'btn-blue',

                        action: function(){



                            $.ajax({



                                url: ajax_object.ajax_url,



                                type: 'post',



                                data: {



                                    action: 'shipped_order',



                                    order: order,



                                    add: addr



                                },



                                success: function(res) {



                                    var response = JSON.parse(res);



                                    var clSts = response.sts ? 'success' : 'err';



                                    $('#notifi').css('display', 'block').removeClass('success err').addClass(clSts);



                                    $('#notification').html(response.msg);

                                        

                                    window.location.reload();



                                }



                            })

                        }

                    },

                    close: function () {

                    }

                }

            });            



        }



    };







    var confirmIsShipped = function() {



        var value = $(this).val();



        var order = $('#Order_ID').val();

        var code = $('#Order_Number').val();



        if (value == 1) {



            jQuery.confirm({

                title: 'Are your sure!',

                content: 'Do you want to update order ' + code + ' to Shipped?',

                type: 'orange',

                typeAnimated: true,

                buttons: {

                    tryAgain: {

                        text: 'Ok',

                        btnClass: 'btn-blue',

                        action: function(){



                            $.ajax({



                                url: ajax_object.ajax_url,



                                type: 'post',



                                data: {



                                    action: 'confirm_ship_order',



                                    value: value,



                                    order: order



                                },



                                success: function(res) {



                                    var response = JSON.parse(res);



                                    var clSts = response.sts ? 'success' : 'err';



                                    $('#notifi').css('display', 'block').removeClass('success err').addClass(clSts);



                                    $('#notifi').html(response.msg);



                                    response.sts && setTimeout(function(){ window.location.reload(); }, 1000);



                                }



                            })

                        }

                    },

                    close: function () {

                        $('#Confirm_Shipped_Order').val(0);

                    }

                }

            });



        }

    };





    var processSyncData = function() {





        jQuery.confirm({

                title: 'Are your sure!',

                content: 'Do you want sync data from google spreadsheet?',

                type: 'orange',

                typeAnimated: true,

                buttons: {

                    tryAgain: {

                        text: 'Ok',

                        btnClass: 'btn-blue',

                        action: function(){



                            $.ajax({



                                url: ajax_object.ajax_url,



                                type: 'post',



                                data: {



                                    action: 'sync_tracking_code_from_google_spreadsheet'



                                },



                                success: function(response) {



                                    var clSts = response.sts ? 'success' : 'err';



                                    $('#notifi').css('display', 'block').removeClass('success err').addClass(clSts);



                                    $('#notifi span').html(response.msg);



                                    response.sts && setTimeout(function(){ window.location.reload(); }, 1000);



                                },



                            })

                        }

                    },

                    close: function () {



                    }

                }

            });



    };

    function storeRatingValue(msg) {
        alert(msg);
    }



})(jQuery);