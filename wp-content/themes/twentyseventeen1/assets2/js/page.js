if($('body').find('.owl-people').length > 0){
    $(".owl-people").owlCarousel({
        navigation : true,
        autoPlay : 5000,
        navigationText : ["<i class='zmdi zmdi-hc-fw'></i>", "<i class='zmdi zmdi-hc-fw'></i>"],
        items : 2,
        itemsDesktop : [1199, 2],
        itemsDesktopSmall : [979, 2],
        itemsTabletSmall : [864, 2],
        itemsTablet : [768, 2],
        itemsMobile : [600, 1]
    });
    $('.owl-people .item').removeClass('col-md-6');
}
if($('body').find('.panelscrool').length > 0){
    if ($(window).width() > 767){
        $.scrollify({
            section:".panelscrool",
            scrollbars:false,
            interstitialSection:".header,.footer",
            before:function(i,panels) {
                var name = panels[i].attr("data-section-id");
                var ref = panels[i].attr("data-section-name");
                $(".pagination_scroll .active").removeClass("active");
                $(".pagination_scroll").find("a[href=\"#" + ref + "\"]").addClass("active");
            },
            afterRender:function() {
                var pagination_scroll = "<ul class=\"pagination_scroll\">";
                var activeClass = "";
                $(".panelscrool").each(function(i) {
                activeClass = "";
                if(i===0) {
                  activeClass = "active";
                }
                pagination_scroll += "<li><a class=\"" + activeClass + "\" href=\"#" + $(this).attr("data-section-name") + "\"><span class=\"hover-text\">" + $(this).attr("data-section-id").charAt(0).toUpperCase() + $(this).attr("data-section-id").slice(1) + "</span></a></li>";
                });
                pagination_scroll += "</ul>";
                $(".home").append(pagination_scroll);
                $(".pagination_scroll a").on("click",$.scrollify.move);
            }
        });
    }
}



