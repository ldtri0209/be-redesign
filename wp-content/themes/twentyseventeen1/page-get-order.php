<?php 
/* 
template name: Get order Data
*/
header("Access-Control-Allow-Origin: *");

if ( is_user_logged_in() ) {
   global $current_user;
      wp_get_current_user();

      $user = $current_user->user_login;
      $user_email  =  $current_user->user_email;
      $user_id = $current_user->ID ;
      $all_meta_for_user = get_user_meta( $user_id );
      $add = $all_meta_for_user['addr'][0];
      $phone = $all_meta_for_user['phone_number'][0];

     
 if($_GET){
	$data = $_GET;
	//print_r($data);
	//$values = array();
	//parse_str($_POST, $values);
	
	$token_rd =  generateRandomString();
	$name= 'DH-'.date('d').date('m');
	// Create post object
	$my_post = array(
	  'post_title'    => wp_strip_all_tags($name.'-'.$token_rd ),
	  'post_status'   => 'publish',
	  'post_type'	=> 'track_order',	
	);
	 		
	// Insert the post into the database
	$post_id = wp_insert_post( $my_post );	
	if($post_id){

	//	$term = array( $tag ); // Correct. This will add the tag with the id 5.
	//	wp_set_post_terms( $post_id, $term, $taxonomy );
		
		// add post_meta
		foreach($data as $k => $v){
			if($k == 'mt'){
				$key = 'action';
			}else if($k=="lc"){
				$key = 'location';
			}
			else if($k=="cat"){
				$key = 'item_category';
			}
			else if($k=="dg"){
				$key = 'dong_goi';
			}
			else if($k=="total"){
				$key = 'total';
			}
			else if($k=="tg"){
				$key = 'p-weight';
			}
			else if($k=="tt"){
				$key = 'volume';
			}
			

			update_post_meta( $post_id, $key, $v );
		}
		update_post_meta( $post_id, 'name', $user );
		update_post_meta( $post_id, 'email', $user_email );
		update_post_meta( $post_id, 'phone', $phone );
		update_post_meta( $post_id, 'address', $add );
		eventks_notification_func();


		$redirect = home_url("/thanks");
		wp_redirect( $redirect );
	}
} 

} else {
$protocol='http';
  if (isset($_SERVER['HTTPS']))
    if (strtoupper($_SERVER['HTTPS'])=='ON')
      $protocol='https';
	$redirect = home_url() . "/login?redirect_to= $protocol://" . $_SERVER["HTTP_HOST"] . urlencode($_SERVER["REQUEST_URI"]);
    wp_redirect( $redirect );
// wp_redirect( wp_login_url($_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]) );
}

add_action( 'wp_insert_post', 'eventks_notification_func', 10, 3 );
function eventks_notification_func($post_id){
	if ( wp_is_post_revision( $post_id ) )
		return;
	$data = $_POST;

	$to = get_bloginfo("admin_email"); 
	
	$subject =  get_bloginfo("name") . '- Một order mới vừa được tạo';
	$body = 'Một order mới vừa được tạo vô trang quản trị để xem thông tin chi tiết';	
	
	$headers = array('Content-Type: text/html; charset=UTF-8', '');
	 
	$succ =  wp_mail( $to, $subject, $body, $headers );	
	
	return $succ;
	
} 
function generateRandomString($length = 8) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


?>



