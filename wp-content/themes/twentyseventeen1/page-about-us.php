<?php

/**

 * The template for displaying all pages

 *

 * This is the template that displays all pages by default.

 * Please note that this is the WordPress construct of pages

 * and that other 'pages' on your WordPress site may use a

 * different template.

 *

 * @link https://codex.wordpress.org/Template_Hierarchy

 *

 * @package WordPress

 * @subpackage Twenty_Seventeen

 * @since 1.0

 * @version 1.0

 */



get_header(); ?>



<?php

$admin_email = get_option('admin_email') ? get_option('admin_email') : 'contact@logistic.com';

$display = wp_get_current_user()->user_firstname . " " . wp_get_current_user()->user_lastname;

?>

<div class="dctrans-aboutus">

	<div class="aboutus-banner">

		<div class="container inside-con">

			<div class="left-side col-md-6">

				<ul class="left-ul">

					<li class="title"><?php _e('Chúng tôi là dịch vụ vận chuyển uy tín DCtrans', 'dctrans_translate') ?></li>

					<li class="description">

						<b>DC</b><?php _e('TRANS Thành lập năm 2003 tại Quảng Châu. Chuyên nhắm đến đối tượng khách hàng là các doanh nghiệp,các cá nhân,tổ chức chuyên làm về thương mại,gia công có nhu cầu nhập khẩu hàng từ Trung Quốc về Việt Nam.', 'dctrans_translate') ?>

						<br/><br/>

						<?php _e('Cung ứng dịch vụ nhập khẩu trọn gói từ Trung Quốc về Việt Nam(toàn bộ giấy tờ thủ tục XNK phía Trung Quốc và Việt Nam, book chỗ trên tàu, book cont, thuê xe kéo cont..v.v. đều do DCTRANS đảm nhiệm)', 'dctrans_translate') ?>

					</li>

				</ul>

			</div>

			<div class="right-side col-md-6">

				<img src="<?php echo  get_template_directory_uri() ?>/images/about-top.jpg">

			</div>

		</div>

	</div>

	<div class="aboutus-stats">

		<div class="stats-inside container">



			<div class="col-md-3 stats-cell">

				<ul class="cell-ul">

					<li class="number">12</li>

					<li class="exp"><?php _e('Văn phòng/ Kho bãi', 'dctrans_translate') ?></li>

				</ul>

			</div>

			<div class="col-md-3 stats-cell">

				<ul class="cell-ul">

					<li class="number">2,430</li>

					<li class="exp"><?php _e('Khách hàng đã sử dụng', 'dctrans_translate') ?></li>

				</ul>

			</div>

			<div class="col-md-3 stats-cell">

				<ul class="cell-ul">

					<li class="number">25,000</li>

					<li class="exp"><?php _e('Đơn hàng vận chuyển mỗi năm', 'dctrans_translate') ?></li>

				</ul>

			</div>

			<div class="col-md-3 stats-cell">

				<ul class="cell-ul">

					<li class="number">12</li>

					<li class="exp"><?php _e('Văn phòng/ Kho bãi', 'dctrans_translate') ?></li>

				</ul>

			</div>

		</div>

	</div>

	<div class="container aboutus-location">

		<div class="main-caption"><?php _e('Địa chỉ liên hệ', 'dctrans_translate') ?></div>

		<div class="location-row">

			<div class="col-md-4 map">

				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d58702.50698182038!2d113.24673880954266!3d23.137085833006633!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3402f8d41bc3a659%3A0xbebebfbbf5fb2ce4!2zVmnhu4d0IFTDuiwgUXXhuqNuZyBDaMOidSwgUXXhuqNuZyDEkMO0bmcsIFRydW5nIFF14buRYw!5e0!3m2!1svi!2s!4v1498438578540" width="100%" height="320" frameborder="0" style="border:0" allowfullscreen></iframe>

			</div>

			<div class="col-md-8 content">

				<ul class="content-ul">

					<li class="title"><?php _e('Trung Quốc', 'dctrans_translate') ?></li>

					<li class="row-text">

						<a class="caption" href="#"><?php _e('Văn phòng chính', 'dctrans_translate') ?>:</a>

						<?php _e('Số 197 đường Trạm Tiền, tòa nhà Cẩm Đo, khu Việt Tú, Tp. Quảng Châu, tỉnh Quảng Đông, Trung Quốc.', 'dctrans_translate') ?>

					</li>

					<li class="row-text">

						<a class="caption" href="#"><?php _e('Kho Đường bộ', 'dctrans_translate') ?>:</a>

						<?php _e('Kho vận tải Việt Nam số 8 trên 1, sau lưng tòa nhà Cẩm Đô, số 197 đường Trạm Tiền, khu Việt Tú,TP. Quảng Châu, Quảng Đông.', 'dctrans_translate') ?>

					</li>

					<li class="row-text">

						<a class="caption" href="#"><?php _e('Điện thoại', 'dctrans_translate') ?>:</a>

						<?php _e('', 'dctrans_translate') ?>1342 768 4407

					</li>

					<li class="row-text">

						<a class="caption" href="#"><?php _e('Kho Đường biển', 'dctrans_translate') ?>:</a>

						<?php _e('Tòa A3 khung công nghiệp Đại Bằng, đường Hoành Linh Nam, đại lộ Khiếu Tâm, thị trấn Thạch Cảnh, khu Bạch Vân,TP. Quảng Châu, tỉnh Quảng Đông.', 'dctrans_translate') ?>

					</li>

					<li class="row-text">

						<a class="caption" href="#"><?php _e('Điện thoại', 'dctrans_translate') ?>:</a>

						<?php _e('', 'dctrans_translate') ?>1392 5000 731

					</li>



				</ul>

			</div>

			<div class="clear-fix"></div>

		</div>



		<div class="location-row">

			<div class="col-md-8 content align-right">

				<ul class="content-ul">

					<li class="title"><?php _e('Việt Nam', 'dctrans_translate') ?></li>

					<li class="row-text">

						<a class="caption" href="#"><?php _e('Văn phòng chính', 'dctrans_translate') ?>:</a>

						<?php _e('Văn phòng đại diện: lầu 5, 911 Nguyễn Trãi, phường 14, Quận 5, Tp.HCM.', 'dctrans_translate') ?>

					</li>

					<li class="row-text">

						<a class="caption" href="#"><?php _e('Kho Đường bộ', 'dctrans_translate') ?>:</a>

						<?php _e('Kho vận tải Việt Nam số 8 trên 1, sau lưng tòa nhà Cẩm Đô, số 197 đường Trạm Tiền, khu Việt Tú,TP. Quảng Châu, Quảng Đông.', 'dctrans_translate') ?>

					</li>

					<li class="row-text">

						<a class="caption" href="#"><?php _e('Điện thoại', 'dctrans_translate') ?>:</a>

						1342 768 4407

					</li>

					<li class="row-text">

						<a class="caption" href="#"><?php _e('Kho Đường biển', 'dctrans_translate') ?>:</a>

						<?php _e('Cạnh khách sạn HILTON, đường Vân Thành Nam 4 Quảng trường WANDA, khu Bạch Vân,TP. Quảng Châu, tỉnh Quảng Đông.', 'dctrans_translate') ?>

					</li>

					<li class="row-text">

						<a class="caption" href="#"><?php _e('Điện thoại', 'dctrans_translate') ?>:</a>

						1372 547 1204

					</li>



				</ul>

			</div>

			<div class="col-md-4 map">

				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d58702.50698182038!2d113.24673880954266!3d23.137085833006633!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3402f8d41bc3a659%3A0xbebebfbbf5fb2ce4!2zVmnhu4d0IFTDuiwgUXXhuqNuZyBDaMOidSwgUXXhuqNuZyDEkMO0bmcsIFRydW5nIFF14buRYw!5e0!3m2!1svi!2s!4v1498438578540" width="100%" height="320" frameborder="0" style="border:0" allowfullscreen></iframe>

			</div>

			<div class="clear-fix"></div>

		</div>





	</div>







	   <div class="clear-fix"></div>





	  <div class="container-fluid ourpartner-container"><!--Our partners Section-->

	    <div class="container">

	      <div class="ourpartner-title"><?php _e('Đối tác của chúng tôi', 'dctrans_translate') ?></div>

	      <div class="partner-logos-container">

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/alipay.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/alibaba.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/tencent.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/dchotel.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/zara.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/kohler.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/visa.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/union.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/maer.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/kangaroo.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/vingroup.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/hoabinh.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/cneastern.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/huawei.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/cntelecom.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/tma.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/gongcha.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/lazada.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/vivo.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/taobao.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/sacombank.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/vietcombank.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/verizon.png" /></div>

	        <div class="col-md-2 col-xs-4 logo"><img src="<?php echo  get_template_directory_uri() ?>/images/logos/cisco.png" /></div>

	      </div>

	    </div>

	  </div><!--/////// End our partner section-->

</div>

<?php get_footer();

