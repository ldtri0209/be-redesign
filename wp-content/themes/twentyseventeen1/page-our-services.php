<?php

/**

 * The template for displaying all pages

 *

 * This is the template that displays all pages by default.

 * Please note that this is the WordPress construct of pages

 * and that other 'pages' on your WordPress site may use a

 * different template.

 *

 * @link https://codex.wordpress.org/Template_Hierarchy

 *

 * @package WordPress

 * @subpackage Twenty_Seventeen

 * @since 1.0

 * @version 1.0

 */



get_header(); ?>



<?php


$admin_email = get_option('admin_email') ? get_option('admin_email') : 'contact@logistic.com';

$display = wp_get_current_user()->user_firstname . " " . wp_get_current_user()->user_lastname;

?>







<div class="clear-fix"></div>

<div class="dctrans-services-container">

	<div class="head-banner">

		<div class="container banner-container">

			<div class="title"><?php _e('Dịch vụ của chúng tôi', 'dctrans_translate') ?></div>

			<div class="slogan col-md-6 col-xs-12">

				<?php _e('Thành lập năm 2003 bởi công ty cổ phần đầu tư và thương mại Ngọc Hoàng Thành. DCTRANS cung cấp các dịch vụ vận tải đường bộ và đường biển từ Trung Quốc về Việt Nam.', 'dctrans_translate') ?>

			</div>

		</div>

	</div>

	<!-- price table-->

	<div class="container price-table">

			<div class="col-md-8 left-side">

				<ul>

					<li class="title"><?php _e('Bảng giá dịch vụ', 'dctrans_translate') ?></li>

					<li class="description"><?php _e('Hãy đăng ký tài khoản trên hệ thống ngay để nhận được bảng giá chi tiết', 'dctrans_translate') ?></li>

				</ul>

			</div>

			<div class="col-md-4 right-side">

				<button class="btn btn-default download-btn"><i class="fa fa-download" aria-hidden="true"></i>
				<a style="color: #ffffff;" href="http://dctrans.asia/wp-content/uploads/pdf/bang-gia-dich-vu.pdf" target="_blank"><?php _e('Tải bảng giá ngay', 'dctrans_translate') ?></a></button>

			</div>

	</div>





	<!-- List down all services here -->

	<div class="container list-services">

		<div class="col-md-4 service-one">

			<ul class="service-ul">

				<li class="image">

					<img src="<?php echo  get_template_directory_uri() ?>/images/service1.png" class="img-responsive" alt="">

				</li>

				<li class="title">
					<a href="http://dctrans.asia/taobao/" class="">
						<?php _e('Vận chuyển Taobao', 'dctrans_translate')?>						
					</a>						
				</li>

				<li class="description">

					<?php _e('DCtrans hỗ trợ tư vấn miễn phí, hướng dẫn tạo tài khoản mua hàng, đặt hàng không qua trung gian - KHÔNG MẤT PHÍ DỊCH VỤ. Tối ưu lợi nhuận cho quý khách hàng.', 'dctrans_translate')?>

				</li>

				<li class="see-more">

					<a href="http://dctrans.asia/taobao/" class=""><?php _e('Xem thêm', 'dctrans_translate')?></a>

				</li>

			</ul>

		</div>



		<div class="col-md-4 service-one">

			<ul class="service-ul">

				<li class="image">

					<img src="<?php echo  get_template_directory_uri() ?>/images/service2.png" class="img-responsive" alt="">

				</li>

				<li class="title">
					<a href="http://dctrans.asia/dich-vu-van-chuyen-duong-bien/" class="">
						<?php _e('Vận chuyển đường biển', 'dctrans_translate')?>
					</a>					
				</li>

				<li class="description">

					<?php _e('Với những mặt hàng có kích thước lớn, cồng kềnh và dễ vỡ như đồ nội thất, xây dựng, hàng điện tử… Vận chuyển hàng Trung Quốc về Việt Nam là lựa chọn tối ưu cho quý khách ...', 'dctrans_translate')?>

				</li>

				<li class="see-more">

					<a href="http://dctrans.asia/dich-vu-van-chuyen-duong-bien/" class=""><?php _e('Xem thêm', 'dctrans_translate')?></a>

				</li>

			</ul>

		</div>



		<div class="col-md-4 service-one">

			<ul class="service-ul">

				<li class="image">

					<a href="http://dctrans.asia/dich-vu-van-chuyen-duong-bo/">
						<img src="<?php echo  get_template_directory_uri() ?>/images/service3.png" class="img-responsive" alt="">
					</a>

				</li>

				<li class="title"><a href="http://dctrans.asia/dich-vu-van-chuyen-duong-bo/"><?php _e('Vận chuyển đường bộ', 'dctrans_translate')?></a></li>

				<li class="description">

					<?php _e('DCtrans vận chuyển hàng từ Trung Quốc về Việt Nam bằng đường bộ với thời gian vận chuyển nhanh, giá thành thấp. DCtrans giúp khách hàng kinh doanh, buôn bán dễ dàng ...', 'dctrans_translate')?>

				</li>

				<li class="see-more">

					<a href="http://dctrans.asia/dich-vu-van-chuyen-duong-bo/" class=""><?php _e('Xem thêm', 'dctrans_translate')?></a>

				</li>

			</ul>

		</div>

	</div>

	<!--Call to action-->

	<div class="call-reg-container">

		<div class="title">

			<?php _e('Để sử dụng dịch vụ DCtrans! Đăng ký ngay', 'dctrans_translate') ?>

		</div>



		<div class="description">

			<?php _e('Là thành viên của Dctrans, bạn có thể đặt hàng Online, tư vấn 24/7 và sử dụng những dịch vụ tốt nhất từ Dctrans ', 'dctrans_translate') ?>

		</div>



		<div class="reg-line">

			<a href="/register" class="reg-btn dctrans-btn"><?php _e('Đăng ký', 'dctrans_translate') ?></a>

		</div>

	</div>







	<div class="service-contact">

		<div class="container">

	      <div class="col-md-6 test-column">

	      	<ul class="test-ul">

	      		<li class="qoute">

	      			<div class="avatar"></div>

	      			<ul>

	      				<li class="name">Alex Hoang</li>

	      				<li class="cmt">

			      			<?php _e('Sứ mệnh của Dctrans
Cung cấp dịch vụ vận chuyển tối ưu cho tất cả khách hàng trong lĩnh vực thương mại với chi phí cạnh tranh và sự tận tụy trong việc hỗ trợ, chăm sóc khách hàng. Trở thành nhà vận chuyển chuyên nghiệp trong lĩnh vực vận chuyển hàng Trung Quốc về Việt Nam ', 'dctrans_translate') ?>

	      				</li>

	      			</ul>

	      			<div class="clear-fix"></div>

	      		</li>

	      	</ul>

	      </div>

		</div>

	</div>

</div>



<?php get_footer();

