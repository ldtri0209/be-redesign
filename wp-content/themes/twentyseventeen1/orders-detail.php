<?php

/*

Template Name: Orders Detail

*/

get_header();

?>

<?php

$hostName = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME'];

$isUser = in_array('subscriber', $current_user->roles);

$Statuses_Array = parseStatusLanguage();

if (!is_array($Statuses_Array)) {$Statuses_Array = array();}

$Locations_Array = get_option("EWD_OTP_Locations_Array");

if (!is_array($Locations_Array)) {$Locations_Array = array();}

$Allow_Order_Payments = get_option("EWD_OTP_Allow_Order_Payments");

$Order = $wpdb->get_row($wpdb->prepare("SELECT * FROM $EWD_OTP_orders_table_name WHERE Order_Number=%s", $_GET['Order_ID']));

$disabled = $isUser || $Order->Order_Status == 'Completed'? 'disabled' : '';



$Order_Statuses = $wpdb->get_results($wpdb->prepare("SELECT os.* FROM $EWD_OTP_order_statuses_table_name os JOIN $EWD_OTP_orders_table_name o ON o.Order_ID = os.Order_ID WHERE Order_Number=%s", $_GET['Order_ID']));

?>

<div class="header-second">
	<div class="container-fluid">
		<div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="">Intlogistic</a></div>
		<a  class="phone-top" href="tel:03723399874" title=""><i class="zmdi zmdi-hc-fw"></i> <span>037-2339-9874</span></a>
		<div class="search">
			<?php if(ICL_LANGUAGE_CODE=='vi'): ?>
				<form action="/vi/tracking-page" method="post" target="_blank" class="pure-form pure-form-aligned">
					<input type="hidden" name="ewd-otp-action" value="track">
					<div class="input-control">
						<input type="text" name="Tracking_Number"  placeholder="Nhập mã theo dõi...">
						<button type="submit" class="btn"><i class="zmdi zmdi-hc-fw"></i></button>
					</div>
				</form>
			<?php elseif(ICL_LANGUAGE_CODE=='zh-hant'): ?>
				<form action="/tracking-page" method="post" target="_blank" class="pure-form pure-form-aligned">
					<input type="hidden" name="ewd-otp-action" value="track">
					<div class="input-control">
						<input type="text" name="Tracking_Number"  placeholder="Input tracking code...">
						<button type="submit" class="btn"><i class="zmdi zmdi-hc-fw"></i></button>
					</div>
				</form>
			<?php endif;?>
		</div>
		<div class="menu-top">
		<?php wp_nav_menu( array( 'theme_location' => 'Top Menu', 'menu_class' => 'nav navbar-nav', 'container'=> null ,'menu' => 'menutop') ); ?>
			
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="manage-content">

	<div class="vertical-center">

		<div class="container">

			<div class="option">
				<div class="title">
					<h2 class="title-page"><?php _e('ORDER DETAIL', 'order-tracking'); ?></h2>
				</div>

				<div class="content">

					<div class="row">

						<div class="col-md-12 col-sm-12 col-xs-12">

							<main id="main" class="site-main" role="main">

								<?php if (!$Order) {

									echo '<h2>Order not found!</h2>';

								} elseif (!$isSupervisor) {?>


									<div id="col-left" class="col-md-6 col-sm-12 col-xs-12">

										<h3 class="title-content"><?php _e("Order Information", 'order-tracking'); ?></h3>

										<p class="um-notice" id="notifi" style="display: none"><i class="um-icon-ios-close-empty" onclick="jQuery(this).parent().fadeOut();"></i><span id="notification"></span></p>

										<div class="col-wrap">

											<div class="form-wrap">

												<form id="addtag" method="post" action="<?php echo $hostName;?>/wp-admin/admin.php?page=EWD-OTP-options&OTPAction=EWD_OTP_EditOrder&DisplayPage=Orders" class="validate" enctype="multipart/form-data">

													<input type="hidden" name="action" value="Edit_Order" />

													<?php wp_nonce_field(); ?>

													<?php wp_referer_field(); ?>

													<input type='hidden' name='Order_ID' id="Order_ID" value='<?php echo $Order->Order_ID; ?>'>

													<input type="hidden" name="Order_Display" value="Yes">

													<input <?php echo $disabled;?> type='hidden' name="Order_Number" id="Order_Number" value="<?php echo stripslashes($Order->Order_Number); ?>" />

													<div class="form-field form-required">

														<label for="Order_Name"><?php _e("Name", 'order-tracking') ?></label>

														<input <?php echo $disabled;?> name="Order_Name" id="Order_Name" type="text" value="<?php echo stripslashes($Order->Order_Name); ?>" size="60" />

													</div>

													<br>

													<div class="form-field">

														<label for="Order_Number"><?php _e("Order Number (Tracking code)", 'order-tracking') ?></label>

														<input type='text' disabled id="Order_Number" value="<?php echo stripslashes($Order->Order_Number); ?>" />

													</div>

													<br>

													<div>

														<label for="Order_Status"><?php _e("Order Status", 'order-tracking') ?></label>

														<?php if (!$disabled) { ?>

															<select name="Order_Status" id="Order_Status" />

															<?php

															foreach ($Statuses_Array as $sts => $Status_Array_Item) { ?>

																<option value='<?php echo $sts ?>' <?php if ($Order->Order_Status == $Status_Array_Item['Status']) {echo "selected='selected'";} ?>><?php echo $Status_Array_Item['Status']; ?></option>

															<?php } ?>

															</select>

														<?php } else {?>

															<input <?php echo $disabled;?> type='text' name="Order_Status" id="Order_Status" value="<?php echo stripslashes($Order->Order_Status); ?>" />

														<?php }?>

													</div>

													<br>

													<div class="form-field">

														<label for="Order_Notes_Public"><?php _e("Order Notes", 'order-tracking') ?></label>

														<input <?php echo $disabled;?> type='text' name="Order_Notes_Public" id="Order_Notes_Public" value="<?php echo stripslashes($Order->Order_Notes_Public); ?>" />

													</div>

													<?php if ($Order->Order_Shipping_Info) { ?>

														<br>

														<div class="form-field">

															<label for="Order_Shipping_Info"><?php _e("Shipment Address", 'order-tracking') ?></label>

															<input <?php echo $disabled;?> type='text' name="Order_Shipping_Info" id="Order_Shipping_Info" value="<?php echo stripslashes($Order->Order_Shipping_Info); ?>" />

														</div>

														<br>

														<div>

															<label for="Confirm_Shipped_Order"><?php _e("Shipment Status", 'order-tracking') ?></label>

															<?php if (! $Order->Confirm_Shipped_Order) { ?>

																<select name="Confirm_Shipped_Order" id="Confirm_Shipped_Order"/>

																<option value='0' <?php echo ! $Order->Confirm_Shipped_Order ? 'selected' : ''?>>Delivering</option>

																<option value='1' <?php echo $Order->Confirm_Shipped_Order ? 'selected' : ''?>>Shipped</option>

																</select>

															<?php } else {?>

																<input disabled type='text' name="Order_Shipping_Info" id="Order_Shipping_Info" value="<?php echo stripslashes($Order->Confirm_Shipped_Order ? 'Shipped' : 'Delivering'); ?>" />

															<?php }?>

														</div>

													<?php }?>

													<p class="submit">

														<?php if (!$disabled) { ?>

															<input type="button" name="submit" id="update-order" class="btn btn-primary default-btn" style="margin: 0" value="<?php _e('Update Order', 'order-tracking') ?>"  />

														<?php }?>

													</p>

												</form>



											</div>



											<br class="clear" />

										</div>

									</div><!-- /col-left -->



									<div id="col-right" class="col-md-6 col-sm-12 col-xs-12">

										<div class="col-wrap">

											<h3 class="title-content"><?php _e("Order Status History", 'order-tracking'); ?></h3>

											<div>

												<table class='table table-striped table-hover' id="order-management">

													<thead>

													<tr>

														<th><?php _e("Order Status", 'order-tracking'); ?></th>

														<th><?php _e("Status History", 'order-tracking'); ?></th>

													</tr>

													</thead>

													<tbody>

													<?php foreach ($Order_Statuses as $Order_Status) { ?>

														<tr>

															<td><?php echo ICL_LANGUAGE_CODE == 'vi' ? parseStatusLanguage($Order_Status->Order_Status) : $Order_Status->Order_Status; ?></td>

															<td><?php echo $Order_Status->Order_Status_Created; ?></td>

														</tr>

													<?php } ?>

													</tbody>

												</table>

											</div>

										</div>

									</div>

								<?php } else {

									echo '<h2>Do not have permission to access!</h2>';


								}?>


							</main>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<!-- .wrap -->

</footer>

<!-- #colophon -->

</div>

<?php get_footer(); ?>

