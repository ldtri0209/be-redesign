<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head>
section and everything up until
<div id="content">
 * * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials * * @package WordPress * @subpackage Twenty_Seventeen * @since 1.0 * @version 1.0 */ ?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>
	">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- Latest compiled and minified CSS -->
    <link href="<?php echo get_template_directory_uri() ?>
	/assets2/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo get_template_directory_uri() ?>
	/assets/css/jquery-confirm.min.css" type="text/css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="<?php echo get_template_directory_uri() ?>
	/assets2/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="<?php echo get_template_directory_uri() ?>
	/assets2/css/material-design-iconic-font.min.css" rel="stylesheet"/>
    <link href="<?php echo get_template_directory_uri() ?>
	/assets2/js/owlCarousel/owl.carousel.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo get_template_directory_uri() ?>
	/assets2/css/main.css?v=1.0.0" rel="stylesheet"/>
    <link href="<?php echo get_template_directory_uri() ?>
	/assets2/css/animate.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>
	/css/main-services.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>
	/css/ourpartner.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>
	/css/services.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>
	/css/dctrans-services.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>
	/css/dctrans-head.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>
	/css/bootstrap-float-label.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>
	/css/footer.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>
	/css/dctrans-newscenter.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>
	/css/dctrans-taobao.css"> <?php wp_head(); ?>
</head>
<?php
$avatar_uri = 'http://dctrans.asia/wp-content/uploads/user.png';
$has_profile_photo = true;
$photo_type = 'um-avatar-uploaded';
$admin_email = get_option('admin_email');
$display = wp_get_current_user()->user_firstname . " " . wp_get_current_user()->user_lastname;
$tigia = get_option('currency');
$fee = get_option('phi_dich_vu');
$tool = get_option('tool_management');?>
<body <?php body_class(); ?>
> <nav class="navbar navbar-default dctrans-mobile-nav" data-spy="affix" data-offset-top="40">
    <div class="container top-nav">
        <div class="col-xs-4 nav-col">
            <div class="dropdown nav-language">
                <?php do_action('wpml_add_language_selector'); ?>
            </div>
        </div>
        <div class="col-xs-4 nav-col">
            <?php if ( is_user_logged_in() ) {?>
                <button class="btn btn-default currency-btn"> 1 ¥ = <?php echo $tigia;?>
                    VND</button>
            <?php } else { ?>
                <button class="btn btn-default currency-btn"><?php _e('Quy đổi tỉ giá', 'dctrans_translate') ?>
                </button>
            <?php } ?>
        </div>
        <div class="col-xs-4 nav-col right">
            <a class="dctrans-link login-link" href="http://dctrans.asia/wp-login.php"><i class="fa fa-sign-in" aria-hidden="true"></i><?php _e('Đăng nhập', 'dctrans_translate') ?>
            </a>
        </div>
    </div>
    <div class="container top-menu">
        <div class="col-xs-6 nav-col">
            <a class="dc-logo" href="#"><img src="<?php echo get_template_directory_uri() ?>/images/dclogo.png" /></a>
        </div>
        <div class="col-xs-3">
        </div>
        <div class="col-xs-3 nav-col right">
            <button class="btn btn-default burger-btn hamburger">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
            <button class="cross-btn">
                <i class="fa fa-times" aria-hidden="true"></i>
            </button>
        </div>
    </div>
    <!--Drop down burger menu-->
    <div class="burger-menu">
        <?php wp_nav_menu( array( 'theme_location' =>
            'Top Menu', 'menu_class' => 'nav navbar-nav h-menu', 'container'=> null ,'menu' => 'menutop') ); ?>
    </div>
    <!-- End drop down burger menu-->
</nav>
<div class="clear-fix">
</div>
<nav class="navbar navbar-default dctrans-top-nav" data-spy="affix">
    <!-- Dctrans top navigator-->
    <div class="container-fluid dctrans-nav">
        <div class="col-md-6 dctrans-nav-left">
            <ul class="nav-ul">
                <li>
                    <div class="dropdown">
                        <?php do_action('wpml_add_language_selector'); ?>
                    </div>
                </li>
                <li class="separator"> &nbsp;</li>
                <li>
                    <div class="dropdown currency-dropdown">
                        <?php if ( is_user_logged_in() ) {?>
                            <button class="btn btn-default dropdown-toggle currency-btn" type="button" id="dropdownMoney" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 1 ¥ = <?php echo $tigia;?>
                                VND <span class="caret"></span></button>
                        <?php } else { ?>
                            <button class="btn btn-default dropdown-toggle currency-btn" type="button" id="dropdownMoney" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><?php _e('Quy đổi tỉ giá', 'dctrans_translate') ?>
                                <span class="caret"></span></button>
                        <?php } ?>
                        <div class="dropdown-menu currency-panel" aria-labelledby="dropdownMoney">
                            <!-- for non-user-->
                            <?php if ( is_user_logged_in() ) {?>
                                <!-- for User-->
                                <div class="title">
                                    <?php _e('Dịch vụ quy đổi tỉ giá', 'dctrans_translate') ?>
                                </div>
                                <div class="description">
                                    <?php _e('Xin vui lòng nhập lưọng tiền cần quy đổi', 'dctrans_translate') ?>
                                    <br>
                                    (<?php _e('phí dịch vụ 50 NDT', 'dctrans_translate') ?>
                                    ).
                                </div>
                                <div class="input">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="money">¥</span>
                                        <input type="text" id="input-money" class="form-control" placeholder="nhập lượng tiền..." aria-describedby="sizing-addon2" value="1">
                                        <input type="hidden" id="money-type" value="Y">
                                        <!--                      hard code-->
                                        <input type="hidden" id="ndt-vnd" value="<?php echo $tigia;?>">
                                    </div>
                                </div>
                                <div class="separator">
                                </div>
                                <!--remove when apply-->
                                <!-- for User-->
                                <div class="description">
                                    <?php _e('Kết quả quy đổi theo yêu cầu', 'dctrans_translate') ?>
                                </div>
                                <div class="result">
                                    ¥ 1 = <?php echo $tigia*$fee;?>
                                    VND
                                </div>
                                <script>
                                    jQuery(document).ready(function() {
                                        jQuery('.currency-dropdown .dropdown-menu').click(function (e) {
                                            e.stopPropagation();
                                        });
                                        jQuery('#money').click(function () {
//                        jQuery(this).html(jQuery(this).html() == '¥' ? 'đ' : '¥');
//                        jQuery('#money-type').val(jQuery(this).html() == '¥' ? 'Y' : 'D');
//                          transferCurrencyMoney();
                                        });
                                        jQuery('#input-money').bind('input', transferCurrencyMoney);
                                        function transferCurrencyMoney() {
                                            var $result = jQuery('.result'),
                                                $inputMoney = jQuery('#input-money');
                                            var type = jQuery('#money-type').val(),
                                                tigia = parseInt(jQuery('#ndt-vnd').val()),
                                                money = parseInt($inputMoney.val()),
                                                fee = <?php echo $fee?>,
                                                result = 0,
                                                rs = '';
                                            if (money <= 0) {
                                                $result.html(rs);
                                                $inputMoney.css('border-color', '#e80000');
                                                return;
                                            }
                                            $inputMoney.css('border-color', '#bbb');
                                            if (type == 'Y') {
                                                result = parseInt((money+fee)*tigia);
                                                rs = '¥ ' + money + ' = ' + result + ' VND';
                                                $result.html(rs);
                                            } else if (type == 'D') {
                                                rs =  money/tigia;
                                                var n = parseFloat(rs);
                                                rs = Math.round(n * 1000)/1000;
                                                rs = money + ' VND' + ' = ¥ ' + rs;
                                                $result.html(rs);
                                            } else {
                                                alert('Chúng tôi chỉ hỗ trợ chuyển đổi giữa NDT và VNĐ.');
                                            }
                                        }
                                    });
                                </script>
                            <?php } else { ?>
                                <div class="title">
                                    <?php _e('Hướng dẫn: Quy đổi tỉ giá', 'dctrans_translate') ?>
                                </div>
                                <div class="description">
                                    <?php _e('Bạn hãy đăng nhập để sử dụng dịch vụ này', 'dctrans_translate') ?>
                                </div>
                                <div class="login">
                                    <a class="btn btn-default" href="http://dctrans.asia/wp-login.php" style="margin: 0"><?php _e('Đăng nhập', 'dctrans_translate') ?>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </li>
                <li class="separator"> &nbsp;</li>
                <li>
                    <a href="#"><i class="fa fa-lg fa-envelope item" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-lg fa-facebook-square item" aria-hidden="true"></i></a>
                </li>
                <li class="separator"> &nbsp;</li>
                <li>
                    <a href="tel:02873043489" class="phone-link dctrans-link"><i class="fa fa-phone-square" aria-hidden="true"></i> 02873043489</a>
                </li>
            </ul>
        </div>
        <div class="col-md-6 dctrans-nav-right no-padding">
            <ul class="user-info">
                <li>
                    <button data-toggle="modal" data-target="#myModal" class="btn btn-default consult-btn dctrans-btn"><?php _e('Gửi câu hỏi', 'dctrans_translate') ?>
                    </button>
                </li>
                <?php if ( is_user_logged_in() ) {?>
                    <?php if (in_array('editor', wp_get_current_user()->
                    roles)) {?>
                        <li class="order-mag-wrapper">
                            <a href="/orders-management" class="order-mag-lnk"><?php _e('Orders Management', 'order-tracking'); ?>
                            </a>
                        </li>
                    <?php } ?>
                    <li>
                        <div class="dropdown clearfix">
                            <img src="<?php echo $avatar_uri; ?>" class="zmdi zmdi-hc-fw user-avatar" /> <button class="btn btn-default dropdown-toggle" type="button" id="username" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php echo $display?>
                                <span class="caret"></span>
                            </button>
                            <ul id="personal" class="dropdown-menu">
                                <?php if ('administrator' == wp_get_current_user()->
                                    roles[0]) {?>
                                    <li class="user-sub-menu"><a href="/wp-admin"><?php _e('Quản trị', 'dctrans_translate') ?>
                                        </a></li>
                                <?php } ?>
                                <li class="user-sub-menu"><a href="/account"><?php _e('Trang Cá Nhân', 'dctrans_translate') ?>
                                    </a></li>
                                <?php if (in_array('subscriber', wp_get_current_user()->
                                roles)) {?>
                                    <li class="user-sub-menu"><a href="/account/orders"><?php _e('Đơn Hàng', 'dctrans_translate') ?>
                                        </a></li>
                                <?php } ?>
                                <li role="separator" class="divider"></li>
                                <li class="user-sub-menu"><a href="http://dctrans.asia/wp-login.php?action=logout"><?php _e('Đăng Xuất', 'dctrans_translate') ?>
                                    </a></li>
                            </ul>
                        </div>
                    </li>
                <?php } else { ?>
                    <!--                    <li>-->
                    <!--                        <a class="dctrans-link" href="http://dctrans.asia/wp-login.php?action=register">--><?php //_e('Đăng ký ngay', 'dctrans_translate') ?>
                    <!--                        </a>-->
                    <!--                    </li>-->
                    <!--                    <li>-->
                    <!--                        <a class="dctrans-link" href="http://dctrans.asia/wp-login.php"><i class="fa fa-sign-in" aria-hidden="true"></i>--><?php //_e('Đăng nhập', 'dctrans_translate') ?>
                    <!--                        </a>-->
                    <!--                    </li>-->
                    <li>
                        <a class="dctrans-link" href="<?php echo $tool?>"><i class="fa fa-sliders" aria-hidden="true"></i> <?php _e('Công cụ quản lí', 'dctrans_translate') ?></a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid dctrans-menu">
        <div class="col-md-6 menu-left">
            <a class="dc-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" ><img src="<?php echo get_template_directory_uri() ?>/images/dclogo.png" /></a>
        </div>
        <!-- <div class="col-md-4 menu-tracking-con">
        <form action="/tracking-page" method="post" target="_blank" class="">
          <div class="input-group">
              <input type="hidden" name="ewd-otp-action" value="track">
              <input type="text" class="form-control tracking-input" name="Tracking_Number"  placeholder="<?php //_e('mã hàng...', 'dctrans_translate') ?>">
              <span class="input-group-btn">
                <button class="btn btn-default dctrans-btn" type="submit"><?php //_e('Tìm ngay', 'dctrans_translate') ?></button>
              </span>
          </div>
        </form>
      </div> -->
        <div class="col-md-6 menu-right">
            <?php wp_nav_menu( array( 'theme_location' =>
                'Top Menu', 'menu_class' => 'nav navbar-nav h-menu', 'container'=> null ,'menu' => 'menutop') ); ?>
        </div>
    </div>
</nav>
<!-- End Dctrans top navigator-->
<div class="clear-fix">
</div>