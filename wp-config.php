<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', 'H:\BK22012018\wp-content\plugins\wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'dc-redesign');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('CONCATENATE_SCRIPTS', false);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'NQDAK]aj(|tV8e?Eu7Rtt~oSI+at}j[!YxjtesNQMASao.<_&~&n3f]yM3^:vq&d');
define('SECURE_AUTH_KEY',  'I{PexZU0fEd(ukO*$2-RWR*t@@T5`kwvowfx@)9V]N%G;Q9g}R8;!KW[|%,c(-gr');
define('LOGGED_IN_KEY',    '=xR9!yu_(:QA-l<pKx3u@QlM#.;*lNbXV0g:xlAzYH/ZkFJn9mFW#dVex}(4YC;l');
define('NONCE_KEY',        ']C%N#i_|+H`vkhtY$%TK5x~#}d 0DrO(LQ)}>K {t$~U^qF}zOulYu?eJ!zi1x K');
define('AUTH_SALT',        'uvmSX25TyL0z8[>UJ^h]9uA~|gIIcGsgTmr]OPycP9`YL9Ye&.F}&7;Ulm8Xz No');
define('SECURE_AUTH_SALT', '7VuLjAs4HcS3xNTvHb@~~a$PHyp{LLd6qQIpP5A2,JdLK1tf{d_Gd}yt~_xSux.I');
define('LOGGED_IN_SALT',   'GWR>D3pTkx8mX_@h2`9ITMJ6R`+o#^K6?@y@h`o6sKU+-YjsdH-!X*D!!Gmg0OBD');
define('NONCE_SALT',       'Z]3P@Le4Gx4OFRpHB8L60)j`AnvS70R<#-tm^^Zno?j3waLeqQV@u@ 1nJELQ!,C');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_MEMORY_LIMIT', '512M');
define( 'WP_MAX_MEMORY_LIMIT' , '512M' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
